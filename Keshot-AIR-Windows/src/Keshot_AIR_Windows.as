package
{
	
	import com.greensock.TweenMax;
	import com.greensock.easing.Expo;
	
	
	import flash.display.Bitmap;
	import flash.display.NativeWindow;
	import flash.display.Screen;
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageAspectRatio;
	import flash.display.StageDisplayState;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.events.NativeWindowBoundsEvent;
	import flash.events.UncaughtErrorEvent;
	import flash.events.ErrorEvent;
	import flash.filesystem.File;
	import flash.geom.Rectangle;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
import flash.text.TextField;
import flash.text.TextFormat;
import flash.ui.Multitouch;
	import flash.ui.MultitouchInputMode;
	import flash.system.Capabilities;
	import flash.desktop.NativeApplication;
	

	//import feathers.themes.MinimalDesktopTheme;
	
	import org.gestouch.core.Gestouch;
	import org.gestouch.extensions.starling.Starling2TouchHitTester;
	import org.gestouch.extensions.starling.StarlingDisplayListAdapter;
	import org.gestouch.input.NativeInputAdapter;
	
	import starling.core.Starling;
	import starling.display.DisplayObject;
	import starling.utils.RectangleUtil;
	import starling.utils.ScaleMode;
	import org.gestouch.extensions.native.NativeDisplayListAdapter;
	
	
	[SWF(width="800", height="800", frameRate="60",  backgroundColor="#FFFFFF")]
	public class Keshot_AIR_Windows extends Sprite
	{
		
		private var CONTENT_PATH:File = File.applicationStorageDirectory;   // También sew debe cambiar en APPMODEL.AS
		
		private var _starling:Starling;
		private var _launchImage:Sprite;
		private var _initConfig:Object;		// archivo de configuacion inicial 

		private var defaultOrientation:String = "";
		
		[ Embed(source="../assets/intro.png") ]
		private var Intro	: Class;
		
		public function Keshot_AIR_Windows()
		{
			
			this.mouseEnabled = this.mouseChildren = false;
			this._launchImage = new Sprite();
			this.addChild(this._launchImage);
			this.addEventListener(Event.ADDED_TO_STAGE, handlerAddedToStage);
			
		}
		
		
		protected function handlerAddedToStage(event:Event):void
		{
			/*
			//Codigo para probar: Una nueva ventana nativa
			var nwio:NativeWindowInitOptions = new NativeWindowInitOptions();
			nwio.renderMode = NativeWindowRenderMode.DIRECT;
			var secondWindow:NativeWindow = new NativeWindow(nwio);
			secondWindow.stage.color = 0xFF0000;
			ScreenManager.openWindowFullScreenOn(secondWindow, 1);
			*/


			trace("LANZADO POR PRIMER VEZ... Mover content");
			var content:File = File.applicationDirectory.resolvePath("content");
			trace("Content exists? "+content.exists, content.url);
			try {
				content.copyTo(File.applicationStorageDirectory.resolvePath("content"),true);
			}
			catch (e:Error)
			{
				trace("ERROR MOVIENDO CARPETA CONTENT A APP-STORAGE "+e.getStackTrace(),e.message)
			}

			loaderInfo.uncaughtErrorEvents.addEventListener( UncaughtErrorEvent.UNCAUGHT_ERROR, uncaughtErrorHandler );
			NativeApplication.nativeApplication.addEventListener(UncaughtErrorEvent.UNCAUGHT_ERROR, uncaughtErrorHandler);


            var appDescriptor:XML = NativeApplication.nativeApplication.applicationDescriptor;
            var ns:Namespace = appDescriptor.namespace();
            var versionNumber:String = appDescriptor.ns::versionNumber;
            trace("versionNumber:", versionNumber);

            var myFormat:TextFormat = new TextFormat();
            myFormat.size = 30;
            myFormat.color = 0xFFFFFF;
            var tf:TextField = new TextField();
			tf.defaultTextFormat = myFormat;
			tf.text = versionNumber;
            this.addChild(tf);



			stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.align = StageAlign.TOP_LEFT;
			var screen_number:uint = Screen.screens.length>2?2:1;
			var bounds:Rectangle = Screen.screens[screen_number-1].bounds;
			trace("BOUNDS",bounds)

			/*
			if( NativeWindow.isSupported)
			{
				stage.nativeWindow.addEventListener(NativeWindowBoundsEvent.MOVE,onMove);
				stage.nativeWindow.addEventListener(NativeWindowBoundsEvent.MOVING,onMove);	
				
				// Redimensionamos la ventana a fullscreen, en caso de presionar escape, saldrá del fullscreen pero mantendrá mismo tamaño
				// Stage fullScreen representará el tamaño del monitor activo al inicio de la ejecución de esta app.  +1 para resolver bug en Windows
				stage.nativeWindow.width = bounds.width;
				stage.nativeWindow.height = bounds.height;
				
				stage.nativeWindow.x = bounds.x+1;
				stage.nativeWindow.y = bounds.y;
			}
			*/
			
			
			
			
			//stage.displayState = StageDisplayState.FULL_SCREEN_INTERACTIVE;
			
			
			
			// Configurar que la app siempre tenga el mismo aspectRatio con que se lanza la app (nunca puede cambiarse durante el juego)
			stage.autoOrients = true;
			defaultOrientation = stage.orientation;
			var aspect:String = this.stage.stageWidth >= this.stage.stageHeight ? StageAspectRatio.LANDSCAPE : StageAspectRatio.PORTRAIT;
			stage.setAspectRatio(aspect);
			
			
			this.loaderInfo.addEventListener(Event.COMPLETE, loaderInfo_completeHandler);
			this.showLaunchImage();
			
		}	
		
		
		// imagen para el lanzamiento de la app.
		private function showLaunchImage():void
		{
			
			
			trace("FullScreen ",stage.fullScreenWidth,stage.fullScreenHeight)
			trace( Capabilities.screenResolutionX, Capabilities.screenResolutionY, Capabilities.localFileReadDisable, Capabilities.screenDPI, Capabilities.touchscreenType )
			var launch:Bitmap =  new Intro();
			this._launchImage.addChild( launch );
			var screenSize:Rectangle = new Rectangle(0, 0, stage.stageWidth, stage.stageHeight);
			var rect:Rectangle = RectangleUtil.fit(launch.bitmapData.rect, screenSize, ScaleMode.SHOW_ALL, true);
			launch.smoothing = true;
			launch.x = rect.x;
			launch.y = rect.y;
			launch.width = rect.width;
			launch.height = rect.height;
			
		}
		
		private function uncaughtErrorHandler( event:UncaughtErrorEvent ):void
		{
			var errorText:String;
			var stack:String;
			if( event.error is Error )
			{
				errorText = (event.error as Error).message;
				stack = (event.error as Error).getStackTrace();
				if(stack != null){
					errorText += stack;
				}
			} else if( event.error is ErrorEvent )
			{
				errorText = (event.error as ErrorEvent).text;
			} else
			{
				errorText = event.text;
			}
			event.preventDefault();
			trace(errorText);
		}

		
		
		
		// Fade out Intro logo
		protected function loaderInfo_completeHandler(event:Event):void
		{
			TweenMax.fromTo(this._launchImage, 5, {colorTransform:{tint:0x000000, tintAmount:0}},{colorTransform:{tint:0x000000, tintAmount:0}, ease:Expo.easeIn, onComplete:loadInitConfiguration});
		}
		
		private function loadInitConfiguration():void
		{
			var file:File = CONTENT_PATH.resolvePath("content/init.json");
			trace("Init file: "+file.nativePath)
			if(file.exists)
			{ 
				
				var request:URLRequest = new URLRequest(file.url); 
				var configLoader:URLLoader = new URLLoader(request); 
				configLoader.addEventListener(Event.COMPLETE, function(event:Event):void 
				{ 
					_initConfig = JSON.parse(configLoader.data);
					trace("Data loaded."); 
					startStarling();
				}); 
				
			}
			else
			{
				trace("ERROR: Config missing")
			}
		}
		
		private function startStarling():void
		{
			Multitouch.inputMode = MultitouchInputMode.TOUCH_POINT;
			
			Starling.multitouchEnabled = false;
			
			var stageSize:Rectangle  = new Rectangle(0, 0, stage.stageWidth, stage.stageHeight);  // que sea al tamaño de nuestro diseño
			var screenSize:Rectangle = new Rectangle(0, 0, stage.stageWidth, stage.stageHeight); //  la resolución de nuestra pantalal
			var viewPort:Rectangle = RectangleUtil.fit(stageSize, screenSize, ScaleMode.SHOW_ALL);

            trace("VIEWPORT:::::: ",viewPort)
			
			this._starling = new Starling(MainApp,stage, viewPort);
			//this._starling.enableErrorChecking = true;
			
			this._starling.addEventListener("rootCreated", starling_rootCreatedHandler);		
			this.stage.addEventListener(Event.DEACTIVATE, stage_deactivateHandler, false, 0, true);
			this._starling.start();
			

			/*
			if(Capabilities.isDebugger)
			{
				this._starling.showStats =  true;
				this._starling.showStatsAt("left","bottom",1);
			}
			else
				this._starling.showStats =  false;
			*/


			this._starling.skipUnchangedFrames = true;
			this._starling.simulateMultitouch = false;
			this._starling.supportHighResolutions = false;
			
			
			Gestouch.inputAdapter = new NativeInputAdapter(stage);
			
			// Evita un quiebre al usar gestos al stage de Flash
			Gestouch.addDisplayListAdapter(flash.display.DisplayObject, new NativeDisplayListAdapter());
			
			
			Gestouch.inputAdapter = new NativeInputAdapter(this.stage);
			Gestouch.addDisplayListAdapter(DisplayObject, new StarlingDisplayListAdapter());
			Gestouch.addTouchHitTester(new Starling2TouchHitTester(), -1);
			
		}
		
		private function starling_rootCreatedHandler(event:Object):void
		{
			trace("PROFILE",this._starling.contextValid,this._starling.profile, this._starling.supportHighResolutions)
			
			if(this._launchImage)
			{
				this.removeChild(this._launchImage);
				this._launchImage = null;
			}
			
			//new MinimalDesktopTheme();
			
			var main:MainApp = Starling.current.root as MainApp;
			main.initConfig = this._initConfig;
			main.start();
			
		}
		
		// Impedir que la app sea movida de pantalla, una vez se haya arrancado
		private function onMove(evt:NativeWindowBoundsEvent):void {
			evt.preventDefault();
		}
		
		
		private function stage_deactivateHandler(event:Event):void
		{
			//this._starling.stop(true);
			//this.stage.addEventListener(Event.ACTIVATE, stage_activateHandler, false, 0, true);
		}
		
		private function stage_activateHandler(event:Event):void
		{
			//this.stage.removeEventListener(Event.ACTIVATE, stage_activateHandler);
			//this._starling.start();
		}
		
		
	}
}