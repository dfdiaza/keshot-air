package
{
	///
	import com.dedosmedia.AppContext;

	import feathers.controls.StackScreenNavigator;

import starling.textures.Texture;

import starling.events.Event;
	
	public class MainApp extends StackScreenNavigator
	{
	 //

		public static const START:String = "startScreen";

		public static const SHOW_START:String = "show"+START;

		
		private var _context:AppContext;
		
		private var _initConfig:Object;
		
		
		
		public function set initConfig(_value:Object):void
		{
			this._initConfig = _value;
		}
		
		public function get initConfig():Object
		{
			return this._initConfig;
		}
		
		
		/*
		 *  ENTRY POINT 
		 *
		 */
		
		public function MainApp()
		{
			this.addEventListener(Event.ADDED_TO_STAGE, addedToStageHandler);
		}
		
		protected function addedToStageHandler(event:Event):void
		{
			this.removeEventListener(Event.ADDED_TO_STAGE, addedToStageHandler);
		}
		
		public function start():void
		{
			this._context = new AppContext(this);
		}
	}
}