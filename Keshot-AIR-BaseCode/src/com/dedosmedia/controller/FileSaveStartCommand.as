package com.dedosmedia.controller
{
	
	//import feathers.examples.weather.services.IFavoriteLocationsService;
	

	import com.dedosmedia.services.IFileService;
	
	import org.robotlegs.starling.mvcs.Command;
	
	import starling.events.Event;
	
	public class FileSaveStartCommand extends Command
	{
		/*
		[Inject]
		public var favoriteLocationsService:IFavoriteLocationsService;
		*/
		
		[Inject]
		public var service:IFileService;
	
		
		
		
		[Inject]
		public var event:Event;
		
		override public function execute():void
		{
			
			trace("execute::FileSaveStartCommand.as "+event);
			
			
			
			
			service.save(event.data.text, event.data.file, event.data.mode, event.data.type);
		}
	}

}