package com.dedosmedia.controller
{
	
	//import feathers.examples.weather.services.IFavoriteLocationsService;
	

	import com.dedosmedia.services.IFileService;
	
	import org.robotlegs.starling.mvcs.Command;
	
	import starling.events.Event;
	
	public class FileCopyStartCommand extends Command
	{
		/*
		[Inject]
		public var favoriteLocationsService:IFavoriteLocationsService;
		*/
		
		[Inject]
		public var service:IFileService;
	
		
		
		
		[Inject]
		public var event:Event;
		
		override public function execute():void
		{
			
			trace("execute::FileCopyStartCommand.as "+event);
			
			
			
			
			service.copy(event.data.src, event.data.dst);
		}
	}

}