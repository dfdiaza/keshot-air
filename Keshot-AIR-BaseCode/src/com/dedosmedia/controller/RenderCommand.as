package com.dedosmedia.controller
{

	
	import com.dedosmedia.model.AssetsModel;
import com.dedosmedia.services.INativeProcessService;
import com.dedosmedia.services.IS3Service;

import flash.filesystem.File;

import org.robotlegs.starling.mvcs.Command;
	
	import starling.events.Event;
	import com.dedosmedia.model.AppModel;
	
	public class RenderCommand extends Command
	{

		
		[Inject]
		public var service:INativeProcessService;
		
		[Inject]
		public var appModel:AppModel;
		
		
		[Inject]
		public var event:Event;
		
		override public function execute():void
		{
			trace("execute::RenderCommand 1 ");


			trace(event.data.executable)
            trace(event.data.processArgs)
			service.start(event.data);



			//service.save(event.data.text, event.data.file, event.data.mode, event.data.type);
		}
	}

}