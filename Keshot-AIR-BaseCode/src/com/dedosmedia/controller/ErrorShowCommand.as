package com.dedosmedia.controller
{
	
	//import feathers.examples.weather.services.IFavoriteLocationsService;

	import com.dedosmedia.services.IFileService;
	
	import org.robotlegs.starling.mvcs.Command;
	
	import starling.events.Event;
	
	public class ErrorShowCommand extends Command
	{	
		
		[Inject]
		public var event:Event;
		
		override public function execute():void
		{
			
			trace("execute::ErrorShowCommand.as "+event.data.text+" - "+event.data.error);
			
			
			
			
			//service.save(event.data.text, event.data.file, event.data.mode);
		}
	}

}