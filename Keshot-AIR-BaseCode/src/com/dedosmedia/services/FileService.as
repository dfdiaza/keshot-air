package com.dedosmedia.services
{
	import com.dedosmedia.events.ErrorEventType;
	import com.dedosmedia.events.FileEventType;
	
	import flash.errors.IOError;
	import flash.events.SecurityErrorEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileStream;
	import flash.utils.ByteArray;
	
	import org.robotlegs.starling.mvcs.Actor;
	
	public class FileService extends Actor implements IFileService
	{
		
		public function copy(src:File, dst:File):void{
			try
			{
				src.copyTo(dst,true);
				trace("file copied!")
				this.dispatchWith(FileEventType.FILE_COPY_COMPLETED);
			}
			catch(e:IOError)
			{
				// no hay permisos o el fichero no existe
				this.dispatchWith(ErrorEventType.ERROR, false, {text:ErrorEventType.ERROR_OPENING_WRITING_FILE, error:e.getStackTrace()});
			}
			catch(e:SecurityError)
			{
				// abriendo fichero en App donde solo se puede leer
				this.dispatchWith(ErrorEventType.ERROR, false, {text:ErrorEventType.ERROR_OPENING_WRITING_FILE, error:e.getStackTrace()} );
			}
		
		}
		
		public function save(text:*, file:File, mode:String, type:String):void
		{
				trace("save::FileService.as ",file.nativePath);
				var stream:FileStream = new FileStream();
				try
				{
					stream.open(file, mode);
					
					switch(type)
					{
						case "text":
							stream.writeUTFBytes(text);
							break;
						case "byteArray":
							var ba:ByteArray = text as ByteArray;
							stream.writeBytes(ba, 0, ba.length);
							break;
					}
					
					stream.close();
					this.dispatchWith(FileEventType.FILE_SAVE_COMPLETED);
				}
				catch(e:IOError)
				{
					// no hay permisos o el fichero no existe
					this.dispatchWith(ErrorEventType.ERROR, false, {text:ErrorEventType.ERROR_OPENING_WRITING_FILE, error:e.getStackTrace()});
				}
				catch(e:SecurityError)
				{
					// abriendo fichero en App donde solo se puede leer
					this.dispatchWith(ErrorEventType.ERROR, false, {text:ErrorEventType.ERROR_OPENING_WRITING_FILE, error:e.getStackTrace()} );
				}
		}
		
		
	}
}