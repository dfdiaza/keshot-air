package com.dedosmedia.services
{
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.utils.ByteArray;

	public interface IFileService
	{
		function save(data:*, file:File, mode:String, type:String):void;
		function copy(src:File, dst:File):void;
		
	}
}