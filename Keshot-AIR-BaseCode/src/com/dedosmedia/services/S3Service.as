package com.dedosmedia.services
{
	import com.dedosmedia.events.ErrorEventType;
	import com.dedosmedia.events.FileEventType;
	import com.dedosmedia.model.AppModel;
	import com.dedosmedia.model.AssetsModel;
	
	import flash.desktop.NativeProcess;
	import flash.desktop.NativeProcessStartupInfo;
	import flash.errors.IOError;
	import flash.events.IOErrorEvent;
	import flash.events.NativeProcessExitEvent;
	import flash.events.ProgressEvent;
	import flash.events.SecurityErrorEvent;
	import flash.events.TimerEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.utils.ByteArray;
	import flash.utils.Timer;
	
	import org.robotlegs.starling.mvcs.Actor;
	
	public class S3Service extends Actor implements IS3Service
	{
		
		[Inject]
		public var assetsModel:AssetsModel;
		
		[Inject]
		public var appModel:AppModel;
		
		
		/// UPLOADING S3
		
		private var cmd:File;
		private var nativeProcess:NativeProcess = new NativeProcess();
		private var nativeProcessStartupInfo:NativeProcessStartupInfo = new NativeProcessStartupInfo();
		private var progressArgs:Vector.<String>;
		private var queue:Vector.<File>;
		private var files:Vector.<File> = new Vector.<File>;
		private var processing:Boolean = false;
		private var timeout:Timer = new Timer(1000,0);
		
		private var config:XML;
		
		public function S3Service():void
		{
			
			nativeProcess.addEventListener(NativeProcessExitEvent.EXIT, onExit);
			nativeProcess.addEventListener(ProgressEvent.STANDARD_OUTPUT_DATA, onOutput);
			nativeProcess.addEventListener(ProgressEvent.STANDARD_ERROR_DATA, onError);
			nativeProcess.addEventListener(IOErrorEvent.STANDARD_OUTPUT_IO_ERROR, onIOError);
			nativeProcess.addEventListener(IOErrorEvent.STANDARD_ERROR_IO_ERROR, onIOError);
			
			/*
			timeout.addEventListener(TimerEvent.TIMER, onTimer);
			timeout.start();
			*/
			
		}
		
		
		private function onTimer(event:TimerEvent):void{
			//upload();
		}
		
		public function upload():void
		{
			config =  new XML(assetsModel.assets.getXml("config").settings.platform.(@os == appModel.os));
			cmd = appModel.projectDirectory;
			cmd = cmd.resolvePath(config.s3_path.text());
			
			if(!processing)
			{
				processing = true;
				var file:File = File.desktopDirectory.resolvePath(config.output_path.text());
				
				var thereAreFiles:Boolean = false;
				if(file.exists)
				{
					queue = getDir(file, ".xml");
					if(queue.length>0){
						//procesar xml
						thereAreFiles = true;
						initProcess();
					}
				}
				
				if(!thereAreFiles)
				{
					trace("No hay ficheros XML para subir")
					processing = false;
				}
			}
			
		}
		
		private function initProcess():void
		{
			var document:XML;
			var stream:FileStream = new FileStream();
			stream.open(queue[0], FileMode.READ);
			document = XML(stream.readUTFBytes(stream.bytesAvailable));
			stream.close();
			
			var path:String = new File(queue[0].parent.nativePath).nativePath;
			var config_file:File = new File(path+File.separator+document.filename.text()+".txt");
			var video_path = path+File.separator+document.filename.text()+document.extension.text();
			var video_file:File = new File(video_path);
			
			trace("path "+path,config_file,video_path,video_file.nativePath)
			
			files.length = 0;
			files.push(config_file) 
			files.push(video_file) 
			files.push(queue[0]) 
			
				
				
			if(video_file.exists)
			{
				
				var ini:String = config.s3_command.text();
				
				
				//var ini:String = 'put "{{video}}" videobooth-empgrouppr/videos/ -cacl:public-read -meta:"x-amz-meta-email:{{email}}|x-amz-meta-subtitle:{{subtitle}}"\r\nq';
				ini = ini.replace(/\n/g, File.lineEnding);
				
				
				
				stream = new FileStream();
				stream.open(config_file, FileMode.WRITE);
				var outputString:String = parse(ini, {video:video_path, email:escape(document.email.text()), subtitle:escape(document.subtitle.text())});
				stream.writeUTFBytes(outputString); 
				stream.close();
				
				trace("WROTE "+config_file.nativePath);
				
				
				init_upload(config_file);
				
				
				
				
				
				
			}
			else
			{
				trace("Error video NO existe... mover xml a ficheros de error");
				moveFilesTo("error");
				processing = false;
			}	
		}
		
		
		private function getDir(desktop:File, filter:String):Vector.<File>
		{
			var getfiles:Array = desktop.getDirectoryListing();
			var _files:Vector.<File> = new Vector.<File>;
			//Push all thi values to the Arraycollection to be diaplayed in the list//
			for (var i:int = 0; i < getfiles.length; i++) { 	
				
				trace((getfiles[i] as File).type+" "+(getfiles[i] as File).name)
				if( (getfiles[i] as File).name.indexOf(filter)!= -1)
				{
					
					// ok type
					_files.push((getfiles[i] as File))
				}
			}
			
			return _files;
		}
		
		// solo un proceso de upload por vez
		private function init_upload(file:File):void{
			
				//cmd = File.desktopDirectory.resolvePath("/bin/ls");
			trace("cmd path "+cmd.nativePath+" > "+cmd.exists)
			try
			{
				
				
				trace(" 1*")
				progressArgs = new Vector.<String>();
				progressArgs.push("-ini:"+file.nativePath);
				
				
				
				trace(" 3* "+cmd)
				nativeProcessStartupInfo.executable = cmd;
				trace(" 4*")
				nativeProcessStartupInfo.arguments = progressArgs;
				
				
				trace("CMD ARGS "+progressArgs+" >>> "+nativeProcessStartupInfo.executable)
				
				if (!nativeProcess.running) {
					nativeProcess.start(nativeProcessStartupInfo);
					
				}
				else
				{
					trace("S3EXPRESS ALREADY UPLOADING");
					
				}
			}
			catch (e:Error)
			{
				
				trace("ERROR: "+e.getStackTrace())
			}
			
		}
		
		private function onExit(event:NativeProcessExitEvent)
		{
			trace("Process exited with "+ event.exitCode);
			
			
			
			if(event.exitCode == 0)
			{
				// OK
				
				
				
				moveFilesTo("done");
				
				queue.splice(0,1);
				
				if(queue.length>0)
				{
					// todavía quedan xmls en fichero
					initProcess();
				}
				else
				{
					trace("NO MAS FICHEROS XML PARA SUBIR. STOP TIMER");
					processing = false;
					//timeout.stop();
				}
				
			}
			else
			{
				// ERROR SUBIENDO EL VÍDEO
				trace("ERROR SUBIENDO EXITCODE "+event.exitCode);
				//moveFilesTo("upload_error");
				processing = false;
			}
			
		}
		
		private function moveFilesTo(folder:String):void
		{
			for(var i=0;i<files.length;i++)
			{
				var original:File = files[i];
				if(original.exists)
				{
					var newFile:File = File.applicationDirectory.resolvePath(new File(files[i].parent.nativePath).nativePath+"/"+folder+"/"+files[i].name); 
					original.moveTo(newFile, true); 
				}
			}
		}
		
		private function onOutput(event:ProgressEvent)
		{
			trace("OUTPUT: "+nativeProcess.standardOutput.readUTFBytes(nativeProcess.standardOutput.bytesAvailable));
		}
		
		private function onError(event:ProgressEvent)
		{
			trace("ERROR: ");	
			processing = false;
		}
		
		private function onIOError(event:IOErrorEvent)
		{
			trace("IO ERROR EVENT ");
			processing = false;
		}
		
		
		
		private function parse(template : String, data : Object): String {
			function replaceFn() : String{ 
				var prop : String = arguments[1];
				return (prop  in data)? data[prop] : '';
			}
			return template.replace(/{{(\w+)}}/g, replaceFn);
		}
		
	}
}