package com.dedosmedia.services
{
	import com.dedosmedia.events.ErrorEventType;
	import com.dedosmedia.events.FileEventType;
import com.dedosmedia.events.NativeProcessEventType;

import flash.errors.IOError;
import flash.events.IOErrorEvent;
import flash.events.NativeProcessExitEvent;
import flash.events.ProgressEvent;

import flash.filesystem.FileStream;
	import flash.utils.ByteArray;

    import flash.filesystem.File;
    import flash.desktop.NativeProcess;
    import flash.desktop.NativeProcessStartupInfo;



	
	import org.robotlegs.starling.mvcs.Actor;
	
	public class NativeProcessService extends Actor implements INativeProcessService
	{

        private var _data:Object;
        private var nativeProcessStartupInfo:NativeProcessStartupInfo = new NativeProcessStartupInfo();
        private var nativeProcess:NativeProcess = new NativeProcess();
        public function start(_data:Object):void
        {

            this._data = _data;
            trace("data ",_data)
            trace(_data.executable, _data.processArgs)
            var file:File = this._data.executable;
            var processArgs:Vector.<String> = this._data.processArgs;

            trace("Nativeprocess service ",NativeProcess.isSupported)
            if(NativeProcess.isSupported)
            {
                nativeProcessStartupInfo.executable = file;
                nativeProcessStartupInfo.workingDirectory = file.parent;

                trace("processArgs: "+processArgs);
                trace("Executable: "+file.nativePath,file.exists)
                nativeProcessStartupInfo.arguments = processArgs;

                if (nativeProcess.running)
                {
                    trace("Close nativeProcess already running");
                    nativeProcess.closeInput();
                    nativeProcess.exit();
                }
                else
                {
                    trace("IS NOT RUNNING....... ")

                }



                try {
                    nativeProcess.start(nativeProcessStartupInfo);
                    nativeProcess.addEventListener(ProgressEvent.STANDARD_OUTPUT_DATA, standardOutputData_handler);
                    nativeProcess.addEventListener(ProgressEvent.STANDARD_ERROR_DATA,standardOutputError_handler);
                    nativeProcess.addEventListener(NativeProcessExitEvent.EXIT, exit_handler);
                    nativeProcess.addEventListener(IOErrorEvent.STANDARD_OUTPUT_IO_ERROR, onIOError);
                    nativeProcess.addEventListener(IOErrorEvent.STANDARD_ERROR_IO_ERROR, onIOError);

                } catch (ae:ArgumentError) {
                    trace("Renderizado: Argument Error2: " + ae.getStackTrace());
                } catch (e:Error) {
                    trace("Renderizado: Error 2: " + e.getStackTrace());
                }

            }
            else
            {
                this.dispatchWith(NativeProcessEventType.NATIVEPROCESS_ERROR);
            }

        }

        private function onIOError(event:IOErrorEvent):void {
            trace("IOERROR: ",event.toString())
        }

        private function exit_handler(event:NativeProcessExitEvent):void {


            nativeProcess.removeEventListener(ProgressEvent.STANDARD_OUTPUT_DATA, standardOutputData_handler);
            nativeProcess.removeEventListener(ProgressEvent.STANDARD_ERROR_DATA,standardOutputError_handler);
            nativeProcess.removeEventListener(NativeProcessExitEvent.EXIT, exit_handler);

            trace("Process exited with "+ event.exitCode, event);
            if(event.exitCode == 0)
            {
                trace("OK DONE ",this._data.executable.nativePath)
                this.dispatchWith(NativeProcessEventType.NATIVEPROCESS_COMPLETED,false,this._data);
            }
            else
            {
                // Error renderizando
                trace("ERROR RENDERIZANDO "+event.exitCode);
                this.dispatchWith(NativeProcessEventType.NATIVEPROCESS_ERROR, false, this._data);
            }
        }

        private function standardOutputError_handler(event:ProgressEvent):void {
            var response:String = nativeProcess.standardError.readUTFBytes(nativeProcess.standardError.bytesAvailable);
            trace("ERROR DATA: ",response);
        }

        private function standardOutputData_handler(event:ProgressEvent):void {
            var response:String = nativeProcess.standardOutput.readUTFBytes(nativeProcess.standardOutput.bytesAvailable);

            trace("OUTPUT DATA: "+response);

        }


		
	}
}