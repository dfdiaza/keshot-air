package com.dedosmedia.utils
{
import flash.events.AsyncErrorEvent;
import flash.events.SecurityErrorEvent;
import flash.net.LocalConnection;
import flash.events.StatusEvent;

public class TwoWayLocalConnection
{
    private var mSenderLocalConnection:LocalConnection;
    private var mReceiverLocalConnection:LocalConnection;
    private var mOtherConnectioName:String;

    public function TwoWayLocalConnection(myConnectionName:String,
                                              otherConnectionName:String, listener:Object)
    {
        mSenderLocalConnection = new LocalConnection();
        mSenderLocalConnection.addEventListener(StatusEvent.STATUS, onSenderLocalConnectionStatus);
        mSenderLocalConnection.addEventListener(AsyncErrorEvent.ASYNC_ERROR,onSenderLocalConnectionAsyncError);
        mSenderLocalConnection.addEventListener(SecurityErrorEvent.SECURITY_ERROR,onSenderLocalConnectionSecurityError);
        mOtherConnectioName = otherConnectionName;
        mReceiverLocalConnection=new LocalConnection();
        try {
            mReceiverLocalConnection.connect(myConnectionName);
        } catch (error:ArgumentError) {
            trace("mReceiverLocalConnection Can't connect...the connection name is already being used by another SWF ",error.message,error.getStackTrace());

            trace("CERRAR CONNECTION")
            try{
                mReceiverLocalConnection.close();
            }
            catch (error:ArgumentError){
                trace("mReceiverLocalConnection Can't close connection...connection is not established ",error.message,error.getStackTrace());
            }
            try {
                mReceiverLocalConnection.connect(myConnectionName);
            } catch (error:ArgumentError) {
                trace("DEFINITIVAMENTW NO PODEMOS RECOENCTAR LA CONEXION QUEDÖ HUERFANA")
            }
        }

        mReceiverLocalConnection.client=listener;
        mReceiverLocalConnection.allowDomain("*");
        mReceiverLocalConnection.addEventListener(StatusEvent.STATUS, onReceiverLocalConnectionStatus);
        mReceiverLocalConnection.addEventListener(AsyncErrorEvent.ASYNC_ERROR,onReceiverLocalConnectionAsyncError);
        mReceiverLocalConnection.addEventListener(SecurityErrorEvent.SECURITY_ERROR,onReceiverLocalConnectionSecurityError);


        trace("DOMAIN: "+mReceiverLocalConnection.domain,mSenderLocalConnection.domain)
    }

    private function onReceiverLocalConnectionAsyncError(event:AsyncErrorEvent):void {
        trace("Receiver AsyncError ",event.text,event.error)
    }

    private function onReceiverLocalConnectionSecurityError(event:SecurityErrorEvent):void {
        trace("Receiver SecurityError ",event.text,event.errorID)
    }

    private function onSenderLocalConnectionSecurityError(event:SecurityErrorEvent):void {
        trace("Sender SecurityError ",event.text,event.errorID)
    }

    private function onSenderLocalConnectionAsyncError(event:AsyncErrorEvent):void {
        trace("Sender AsyncError ",event.text,event.error)
    }

    public function onSenderLocalConnectionStatus(statusEvent:StatusEvent):void
    {
        trace("onSenderLocalConnectionStatus ",statusEvent.level,statusEvent.code);

        switch (statusEvent.level) {
            case "status":
                trace("LocalConnection.send() succeeded",statusEvent.code);
                break;
            case "error":
                trace("LocalConnection.send() failed",statusEvent.code);
                break;
        }
    }

    public function send(domain:String, method:String, obj:Object):void
    {
        trace("SEND:: ",domain+":"+mOtherConnectioName, method)
        mSenderLocalConnection.send(domain+":"+mOtherConnectioName, method, obj);
    }

    public function onReceiverLocalConnectionStatus(statusEvent:StatusEvent):void
    {
        trace("onReceiverLocalConnectionStatus ",statusEvent.level,statusEvent.code);
        switch (statusEvent.level) {
            case "status":
                trace("Received succeeded",statusEvent.code);
                break;
            case "error":
                trace("Received failed",statusEvent.code);
                break;
        }
    }


}
}