package com.dedosmedia.utils
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.geom.Rectangle;
	import flash.utils.ByteArray;
	
	import starling.utils.AssetManager;
	
	
	/*
	* Para usarlo: _assets.keepBitmapData(projectDirectory.resolvePath("image/frame.png").url);
	*
	*    assets.getBitmapData('AtlasName', 'ElementName'); // for an atlas
	     assets.getBitmapData('frame') // sin atlas
	*/
	
	public class CustomAssetManager extends AssetManager
	{
		private var _bitmapDataToKeep:Vector.<String>;
		private var _bitmapDataList:Object;
		
		
		
		
		
		public function CustomAssetManager(scaleFactor:Number=1, useMipmaps:Boolean=false)
		{
			super(scaleFactor, useMipmaps);			
		}
		
		
		
		
		/**
		 * Adds a bitmap to keep in memory as bitmap data
		 * @param	$textureUrl			Url of the image to load
		 */
		public function keepBitmapData($textureUrl:String):void {
			if (!_bitmapDataToKeep) {
				_bitmapDataToKeep = new Vector.<String>();
			}
			_bitmapDataToKeep.push($textureUrl);
		}
		
		/**
		 * Get bitmap data of a resource
		 * @param	$texture		name of the loaded file
		 * @param	$name			if the loaded file is an atlas name of the element in the atlas
		 */
		public function getBitmapData($texture:String, $name:String = null):BitmapData {
			if (!$name) {
				if(_bitmapDataList != null)
				{
					return(_bitmapDataList[$texture]);
				}
				else
				{
					return null;
				}
				
			}
			var area:Rectangle = getTextureAtlas($texture).getRegion($name);
			var res:BitmapData = new BitmapData(area.width, area.height, true, 0x00000000);
			var pixels:ByteArray = _bitmapDataList[$texture].getPixels(area);
			pixels.position = 0;
			res.setPixels(new Rectangle(0, 0, res.width, res.height), pixels);
			return res;
		}
		
		override protected function loadRawAsset(rawAsset:Object, onProgress:Function, onComplete:Function):void {
			super.loadRawAsset(rawAsset, onProgress, function($asset:Object):void {
				//if (_bitmapDataToKeep && _bitmapDataToKeep.indexOf(rawAsset as String) > -1) {
				if(rawAsset.indexOf("keepBitmapData")!=-1) {
					addBitmapData(getName(rawAsset), $asset as Bitmap);
				}
				onComplete($asset);
			});
		}
		
		private function addBitmapData($name:String, $data:Bitmap):void {
			if (!_bitmapDataList) {
				_bitmapDataList = new Object();
			}
			_bitmapDataList[$name] = $data.bitmapData.clone();
		}

	}	
}