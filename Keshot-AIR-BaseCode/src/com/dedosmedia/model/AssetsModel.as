package com.dedosmedia.model
{
	import com.dedosmedia.utils.CustomAssetManager;
	import org.robotlegs.starling.mvcs.Actor;

	
	public class AssetsModel extends Actor
	{
		
		// AssetManager
		private var _assets:CustomAssetManager = new CustomAssetManager();
		
		public function AssetsModel()
		{
		}
		
		
		public function get assets():CustomAssetManager
		{
			return this._assets;
		}
		
	}
}