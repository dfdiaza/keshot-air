package com.dedosmedia.model
{
	
	//import com.dedosmedia.interfaces.IFWVideoEncoder;

import com.dedosmedia.model.vo.ConfigVO;
import com.dedosmedia.utils.TwoWayLocalConnection;

import flash.display.BitmapData;
	import flash.filesystem.File;
	import flash.geom.Rectangle;
	
	import org.robotlegs.starling.mvcs.Actor;
	
	public class AppModel extends Actor
	{

        public function AppModel()
        {
            this._config = new ConfigVO();
        }

		/*
		/*   config: Contiene un Value Object con toda la información de configuración importante cargada de un json
		 */
        private var _config:ConfigVO;
        public function get config():ConfigVO
        {
            return this._config;
        }
        public function set config(_config:ConfigVO):void
        {
            this._config = _config;
        }

		/*
		 * aerender:  Indica el Path hacia el ejecutable de aerender  (solo para envío de renderizado de composiciones)
		 */
        private var _aerender:File;
        public function get aerender():File
        {
            return this._aerender;
        }
        public function set aerender(_aerender:File):void
        {
            this._aerender = _aerender;
        }

		/*
		 * afterfx:  Indica el Path hacia el ejecutable de afterfx  (Para enviar scripts como de Purge Memory, etc)
		 */
        private var _afterfx:File;
        public function get afterfx():File
        {
            return this._afterfx;
        }
        public function set afterfx(_afterfx:File):void
        {
            this._afterfx = _afterfx;
        }

		/*
		 * projectDirectory:  Indica el Path hacia el directorio que contiene el proyecto en ejecución
		 */
        private var _projectDirectory:File;
        public function get projectDirectory():File
        {
            return this._projectDirectory;
        }

        public function set projectDirectory(file:File):void
        {
            this._projectDirectory = file;
        }

		/*
		 * content:  Indica el Path hacia se guarda la carpeta content, con toda la configuración del proyecto
		 */
        private var _content:File = new File("app-storage:/content"); //app:/content
        public function get content():File
        {
            return this._content;
        }
        public function set content(file:File):void
        {
            this._content = file;
        }


		/*
		 * input: Indica el Path hacia la primera foto (input0) capturada en la sessión en la versión Flash.
		 */
        private var _input:File;

        public function get input():File
        {
            return this._input
        }
        public function set input(_input:File)
        {
            this._input = _input;
        }

		/*
		 * input: Indica el Path hacia la carpeta base del proyecto trabajado de Keshot
		 */
        private var _basePath:File;

        public function get basePath():File
        {
            return this._basePath
        }
        public function set basePath(_basePath:File)
        {
            this._basePath = _basePath;
        }







        // Ruta al directorio de proyecto actualmente en ejecución
		private var _os:String;


		private var _size:Rectangle;
		
		private var _design_size:Rectangle;
		
		
		//private var _playerNumber:uint = 0;
		private var _gamePlan:Object;
		private var _currentPlayer:Object;









        private var _outputFrames:Number = 1;

		public function get outputFrames():Number
		{
			return this._outputFrames;
		}

        public function set outputFrames(_outputFrames:Number):void
        {
            this._outputFrames = _outputFrames;
        }
		
		
		private var _localConnection:TwoWayLocalConnection;


		private var _fileExtension:String;
		public function get fileExtension():String
		{
			return this._fileExtension
		}
		public function set fileExtension(_fileExtension:String)
		{
			this._fileExtension = _fileExtension;
		}







        // Archivo de configuración inicial
		
		
		private var _timestamp:String;
		
	
		private var _snapshot:BitmapData;

		private var _contentUpdated:Boolean	= false;			//Indica si en esta sesión ya se ha movido el contenido al proyecto AIR
		public function get contentUpdated():Boolean
		{
			return this._contentUpdated;
		}
        public function set contentUpdated(_contentUpdated:Boolean):void
        {
            this._contentUpdated = _contentUpdated;
        }
		
		public function get snapshot():BitmapData
		{
		
			return this._snapshot;
		}
		
		public function set snapshot(_snapshot:BitmapData):void
		{
			if(this._snapshot)
			{
				this._snapshot.dispose();
				this._snapshot = null;
			}
			this._snapshot = _snapshot;
		}


        public function get localConnection():TwoWayLocalConnection
        {
            return this._localConnection;
        }

        public function set localConnection(_localConnection:TwoWayLocalConnection):void
        {
            this._localConnection = _localConnection;
        }

		public function get timestamp():String
		{
			return this._timestamp;
		}
		
		public function set timestamp(_timestamp:String):void
		{
			this._timestamp = _timestamp;
		}
		

		
		/*
		public function get playerNumber():uint
		{
			return this._playerNumber;
		}
		
		public function set playerNumber(_playerNumber:uint):void
		{
			this._playerNumber = _playerNumber;
		}
		*/
		
		
		public function get currentPlayer():Object
		{
			return this._currentPlayer;
		}
		
		public function set currentPlayer(_currentPlayer:Object):void
		{
			this._currentPlayer = _currentPlayer;
		}
		
		public function get gamePlan():Object
		{
			return this._gamePlan;
		}
		
		public function set gamePlan(_gamePlan:Object):void
		{
			this._gamePlan = _gamePlan;
		}
		
		
		

		

		
		public function get os():String
		{
			return this._os;
		}
		
		public function set os(_os:String):void
		{
			this._os = _os;
		}
		
		/*
		public function get size():Rectangle
		{
			return this._size;
		}
		
		public function set size(_value:Rectangle):void
		{
			this._size = _value;
		}
		*/
		
		
		public function get design_size():Rectangle
		{
			return this._design_size;
		}
		
		public function set design_size(_value:Rectangle):void
		{
			this._design_size = _value;
		}
		
		
		
	}
}


