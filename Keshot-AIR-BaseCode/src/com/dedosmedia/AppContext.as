package com.dedosmedia
{

	import com.dedosmedia.controller.ErrorShowCommand;
	import com.dedosmedia.controller.FileCopyStartCommand;
	import com.dedosmedia.controller.FileSaveStartCommand;
	import com.dedosmedia.controller.PlaySoundCommand;
	import com.dedosmedia.controller.StartupCommand;
	import com.dedosmedia.controller.RenderCommand;
	import com.dedosmedia.events.ErrorEventType;
	import com.dedosmedia.events.FileEventType;
import com.dedosmedia.events.NativeProcessEventType;
import com.dedosmedia.events.SoundEventType;
	import com.dedosmedia.model.AppModel;
	import com.dedosmedia.model.AssetsModel;
	import com.dedosmedia.services.FileService;
	import com.dedosmedia.services.IFileService;
import com.dedosmedia.services.INativeProcessService;
import com.dedosmedia.services.IS3Service;
	import com.dedosmedia.services.S3Service;
	
	
	import com.dedosmedia.views.components.*;
	import com.dedosmedia.views.mediators.*;
	

	import com.dedosmedia.views.mediators.MainAppMediator;

	import org.robotlegs.starling.base.ContextEventType;
	import org.robotlegs.starling.core.IInjector;
	import org.robotlegs.starling.mvcs.Context;
	
	import starling.display.DisplayObjectContainer;
	
	import com.dedosmedia.services.NativeProcessService;
	
	
	public class AppContext extends Context
	{
		public function AppContext(contextView:DisplayObjectContainer=null, autoStartup:Boolean=true)
		{
			super(contextView, autoStartup);
			
		}
		
		override protected function get injector():IInjector
		{
			return super.injector;
		}
		
		override protected function set injector(value:IInjector):void
		{
			super.injector = value;
		}
	
		override public function startup():void
		{
			this.injector.mapSingleton(AppModel);
			this.injector.mapSingleton(AssetsModel);
			
			this.mediatorMap.mapView(MainApp, MainAppMediator);
			
			this.mediatorMap.mapView(StartScreen, StartScreenMediator);

			//this.injector.mapSingletonOf(IS3Service, S3Service);
			this.injector.mapSingletonOf(IFileService, FileService);

            this.injector.mapSingletonOf(INativeProcessService, NativeProcessService);




            this.commandMap.mapEvent(FileEventType.FILE_SAVE_START, FileSaveStartCommand );
			/*
            this.commandMap.mapEvent(FileEventType.FILE_COPY_START, FileCopyStartCommand );
            this.commandMap.mapEvent(FileEventType.FILE_UPLOAD_START, RenderCommand )
            */
			
			this.commandMap.mapEvent(ErrorEventType.ERROR, ErrorShowCommand );
			this.commandMap.mapEvent(SoundEventType.PLAY_SOUND, PlaySoundCommand )


            this.commandMap.mapEvent(NativeProcessEventType.NATIVEPROCESS_START, RenderCommand);

			
			this.commandMap.mapEvent(ContextEventType.STARTUP, StartupCommand);
			
		
			this.dispatchEventWith(ContextEventType.STARTUP);
			//super.startup();
		}
	}
}