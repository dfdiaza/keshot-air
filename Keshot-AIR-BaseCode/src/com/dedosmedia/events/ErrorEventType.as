package com.dedosmedia.events
{
	public class ErrorEventType
	{
		public static const ERROR:String = "error";
	
		public static const ERROR_OPENING_WRITING_FILE:String = "Error abriendo o escribiendo fichero en disco.";
		public static const ERROR_STAGE_VIDEO_AVAILABILITY:String = "Error, StageVideo no está disponible.";
	}
}


