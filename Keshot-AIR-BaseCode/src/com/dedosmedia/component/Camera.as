package com.dedosmedia.component
{
	import com.dedosmedia.events.ErrorEventType;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Stage;
	import flash.display.StageQuality;
	import flash.events.Event;
	import flash.events.StageVideoAvailabilityEvent;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.media.Camera;
	import flash.media.StageVideo;
	import flash.media.StageVideoAvailability;
	
	import feathers.controls.ImageLoader;
	
	import starling.events.Event;
	import starling.textures.Texture;
	import starling.utils.RectangleUtil;

	public class Camera extends ImageLoader
	{
		
		private var _stage:Stage;
		private var _stageVideo:StageVideo;
		private var _camera:flash.media.Camera;
		private var _cameraIndex:String;
		private var _fps:Number = 30;
		private var _quality:int = 100;
		private var _bandwidth:int = 0;
		private var _frameBmd:BitmapData;
		
		private var _mode:String
		
		// Stream de la cámara sin escalar
		private var _streamBmd:BitmapData;										// BMD Entrada de la webcam
		private var _streamRect:Rectangle = new Rectangle(0,0,0,0);   	// Resolución de la webcam
		
		// Stream de la cámara escalado
		private var _streamScaledBmd:BitmapData;								// BMD Entrada de la webcam escalado
		private var _streamScaledRect:Rectangle;								// Resolución de la webcam escalado
		private var _streamScaledPosition:Point;								// POsicion del stream escalado en el vp
		
		// viewport de la cámara
		private var _viewportBmd:BitmapData;									// El viewport bmd al tamaño original sin escalado.
		private var _viewportRect:Rectangle = new Rectangle(0,0,0,0);		// El viewport rect que queremos ver
		private var _viewportPosition:Point = new Point(0,0)					// posicion del viewport en la imagen de salida
			
			
		//output compuesto
		private var _outputBmd:BitmapData;										// BMD Salida final
		private var _outputRect:Rectangle = new Rectangle(0,0,0,0)				// Tamaño de la imagen de salida, lo da la textura
		
			
		private var _scaleFactor:Number = 1;									//  Que tanto se escala la salida de la cámara en modo "showAll"
		private var _scaleMatrix:Matrix = new Matrix();
		
	
		private var zeroPoint:Point = new Point(0,0);

		
		public function Camera(_stage:Stage)
		{
			this._stage = _stage;
		}
		
		public function start(_cameraIndex:String, _frameBmd:BitmapData = null):void
		{
		
			this._cameraIndex = _cameraIndex;
			this._streamBmd = new BitmapData(this._streamRect.width,this._streamRect.height, true, 0x00000000);
			this._viewportBmd = new BitmapData(this._viewportRect.width,this._viewportRect.height, true, 0x00000000);
			this._outputBmd = new BitmapData(Texture(this.source).width, Texture(this.source).height);
			// stream scaled
			this._streamScaledRect = RectangleUtil.fit(this._streamRect,this._viewportRect,this._mode,false);
			this._scaleFactor = this._streamRect.width/this._streamScaledRect.width;
			this._scaleMatrix.scale(1/this._scaleFactor, 1/this._scaleFactor); 
			if(this._scaleFactor != 1)
			{
				this._streamScaledBmd = new BitmapData(this._streamBmd.width/this._scaleFactor, this._streamBmd.height/this._scaleFactor); 
			}

			if(this._streamScaledRect.x < 0 || this._streamScaledRect.y < 0)
			{
				// Cambiar las coordenadas x,y del viewpoer rect
				this._viewportRect.x = Math.abs(this._streamScaledRect.x);
				this._viewportRect.y = Math.abs(this._streamScaledRect.y);
				this._streamScaledPosition = this.zeroPoint;
			}
			else
			{
				// centrar el stream scalado en el viewport, mostrando todo
				var temp:Rectangle = RectangleUtil.fit(this._streamScaledRect,this._viewportRect,"noBorder",false);
				this._streamScaledPosition = new Point(temp.x, temp.y)
			}
			if(_frameBmd)
				this._frameBmd = _frameBmd.clone();
			this._stage.addEventListener(StageVideoAvailabilityEvent.STAGE_VIDEO_AVAILABILITY, onStageVideoState);
				
		}
		/*
		public function set frameBmd(_frameBmd:BitmapData):void
		{
			this._frameBmd = _frameBmd.clone();
		}
		
		public function get frameBmd():BitmapData
		{
			return this._frameBmd;
		}
		*/
		
		
		public function set mode(_mode:String):void
		{
			this._mode = _mode;
		}
		
		public function get mode():String
		{
			return this._mode;
		}
		
		public function set streamRect(_streamRect:Rectangle):void
		{
			this._streamRect = _streamRect;
		}
		
		public function get streamRect():Rectangle
		{
			return this._streamRect;
		}
		
		public function set viewportRect(_viewportRect:Rectangle):void
		{
			this._viewportRect = _viewportRect;
		}
		
		public function get viewportRect():Rectangle
		{
			return this._viewportRect;
		}
		
		public function set viewportPosition(_viewportPosition:Point):void
		{
			this._viewportPosition = _viewportPosition;
		}
		
		public function get viewportPosition():Point
		{
			return this._viewportPosition;
		}
		
		public function set fps(_fps:Number):void
		{
			this._fps = _fps;
		}
		
		public function get fps():Number
		{
			return this._fps;
		}
		
		public function set bandwidth(_bandwidth:int):void
		{
			this._bandwidth = _bandwidth;
		}
		
		public function get bandwidth():int
		{
			return this._bandwidth;
		}
		
		public function set quality(_quality:int):void
		{
			this._quality = _quality;
		}
		
		public function get quality():int
		{
			return this._quality;
		}

		
		private function onStageVideoState(event:StageVideoAvailabilityEvent):void       
		{       
			
			if(event.availability == StageVideoAvailability.AVAILABLE)
			{			
				
				trace(flash.media.Camera.names, this._cameraIndex)
				// obtener el stream de la camara
				this._camera = flash.media.Camera.getCamera(this._cameraIndex);
				this._camera.setMode(this._streamRect.width,this._streamRect.height,this._fps);
				this._camera.setQuality(this._bandwidth,this._quality);
				this._stageVideo = this._stage.stageVideos[0];
				this._camera.addEventListener(flash.events.Event.VIDEO_FRAME, onVideoFrame);
				this._stageVideo.attachCamera(this._camera);
			}
			else
			{
				trace("NO HAY STAGEVIDEO DISPONIBLE");
				this.dispatchEventWith(ErrorEventType.ERROR, false, ErrorEventType.ERROR_STAGE_VIDEO_AVAILABILITY);
			}
		}
		
		protected function onVideoFrame(e:flash.events.Event):void {
			
			
			this._camera.drawToBitmapData(this._streamBmd);
			
			// Si es escalado se redimensiona al tamaño de viewport
			if(this._scaleFactor != 1)
			{
				this._streamScaledBmd.draw(this._streamBmd, this._scaleMatrix, null, null, null, true);
			}
			else
			{
				//trace("Stream no se debe escalar")
				this._streamScaledBmd = this._streamBmd.clone();
			}
			
			
			// recortamos el stream, y lo ajsutamos al viewport
			// si coordenadas negativas, al stream lo recortamnos así
			if(this._streamScaledRect.x < 0 || this._streamScaledRect.y < 0)
			{
				this._viewportBmd.copyPixels(this._streamScaledBmd,this._viewportRect,this._streamScaledPosition); // position: 0,0
			}
			else
			{
				this._viewportBmd.copyPixels(this._streamScaledBmd,this._streamScaledBmd.rect,this._streamScaledPosition);	// position, la q diga el escalado
			}
			
			
			//poner el frame
			if(this._frameBmd)
			{
				//trace("pone frame")
				this._outputBmd.copyPixels(this._viewportBmd,this._viewportBmd.rect,this._viewportPosition);
				this._outputBmd.copyPixels(this._frameBmd,this._frameBmd.rect,this.zeroPoint,null,null,true);
			}
			
			this.source.root.uploadBitmapData(this._outputBmd)
				
			trace("dispatch "+this._outputBmd.rect)
			this.dispatchEventWith("VIDEO_FRAME", true, this._outputBmd);
			
		}
		
		
		
		public function destroy():void
		{
			this._stageVideo.attachCamera(null);
			
			
			if(this._stage.hasEventListener(StageVideoAvailabilityEvent.STAGE_VIDEO_AVAILABILITY))
			{
				this._stage.removeEventListener(StageVideoAvailabilityEvent.STAGE_VIDEO_AVAILABILITY, onStageVideoState);
			}
			
			if(this._camera.hasEventListener(flash.events.Event.VIDEO_FRAME))
			{
				this._camera.removeEventListener(flash.events.Event.VIDEO_FRAME, onVideoFrame);
			}
			
			if(this._streamBmd) {
				this._streamBmd.dispose();
				this._streamBmd = null;
			}		
			
			if(this._streamScaledBmd) {
				this._streamScaledBmd.dispose();
				this._streamScaledBmd = null;
			}	
			
			if(this._frameBmd){
				this._frameBmd.dispose();
				this._frameBmd = null;
			}
			
			if(this._outputBmd){
				this._outputBmd.dispose();
				this._outputBmd = null;
			}
			
			if(this._viewportBmd){
				this._viewportBmd.dispose();
				this._viewportBmd = null;
			}
			
			
		}
	}
}