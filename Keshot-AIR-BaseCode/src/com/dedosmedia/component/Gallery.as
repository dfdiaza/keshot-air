package com.dedosmedia.component
{
	import feathers.controls.ImageLoader;
	import feathers.controls.ScrollPolicy;
	import feathers.controls.ScrollInteractionMode;
	import feathers.layout.HorizontalLayout;
	import feathers.layout.VerticalAlign;
	import feathers.layout.HorizontalAlign;
	import feathers.controls.List;
	import feathers.controls.ScrollBarDisplayMode;
	import feathers.controls.renderers.IListItemRenderer;
	import feathers.controls.renderers.DefaultListItemRenderer;
	
	import starling.events.Event;
	
	public class Gallery extends List
	{
		

		
	
		
		
		private var _gallery_layout:HorizontalLayout = new HorizontalLayout();
		private var _gap:Number = 0;
		private var _padding:Number = 0;
		private var _item:Array = new Array();
		
		private var _image:Vector.<ImageLoader> = new Vector.<ImageLoader>();
		
		
		public function Gallery()
		{
			super();
			
			this.styleProvider = null;
			this._gallery_layout.verticalAlign = VerticalAlign.MIDDLE;
			this._gallery_layout.hasVariableItemDimensions = false;
			this._gallery_layout.useVirtualLayout = true;
		//	this._layout.typicalItemHeight = 551;
//			this._layout.typicalItemWidth = 806;
			this.layout = this._gallery_layout;
			
			
			this.snapScrollPositionsToPixels = true; 
			this.horizontalScrollPolicy = ScrollPolicy.AUTO;
			this.verticalScrollPolicy = ScrollPolicy.AUTO;
			this.interactionMode = ScrollInteractionMode.TOUCH;
			this.hasElasticEdges = true;
			this.padding = 0;
			this.scrollBarDisplayMode = ScrollBarDisplayMode.NONE;
			
			this.itemRendererFactory = function():IListItemRenderer
			{
				var renderer:DefaultListItemRenderer = new DefaultListItemRenderer();
				renderer.styleProvider = null;
				
				renderer.hasLabelTextRenderer = false;
				//renderer.iconSourceField = "texture";
				renderer.iconField = "texture";
				renderer.isQuickHitAreaEnabled = true;
				
				
				return renderer;
			};
			
			//this.addEventListener( Event.CHANGE, list_changeHandler );
			
		}
		
		private function list_triggeredHandler( event:Event ):void
		{
			var list:List = List( event.currentTarget );
			trace( "selectedIndex:", list.selectedIndex );
		}
		
		public function set gap(_gap:Number):void
		{
			this._gap = _gap;
			this._gallery_layout.gap = _gap;
		}
		
		public function get gap():Number
		{
			return this._gap;
		}
		
		public function set item(_item:Array):void
		{
			trace("????? SET ITEM")
			this._item = _item;		
			while(this.numChildren>0)
				this.removeChildAt(0,true);
			
			for(var y:int = 0, y_max:int = this._item.length; y < y_max; y++)
			{
				this.addChild(this._item[y]);
			}
		}
		
		public function get item():Array
		{
			return this._item;
		}
		
		public override function dispose():void
		{
			super.dispose();
			
			this.removeEventListener( Event.TRIGGERED, list_triggeredHandler );
			
			//this.backgroundSkin.dispose();
			trace("**** SE NECESITA REMOVER backgrounSkin on gallery?")
		}
		/*
		var skin:Image = new Image( texture );
		skin.scale9Grid = new Rectangle( 2, 2, 1, 6 );
		container.backgroundSkin = skin;
		*/

		
	}
}