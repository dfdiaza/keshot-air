package com.dedosmedia.component
{
	import flash.display.BitmapData;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.media.Camera;
	
	import starling.display.Image;
	import starling.textures.Texture;
	import starling.utils.RectangleUtil;
	import starling.display.Sprite;

	public class CameraTexture extends Sprite
	{
		
		private var _camera:flash.media.Camera;
		private var _cameraIndex:String;
		private var _fps:Number = 30;
		private var _quality:int = 100;
		private var _bandwidth:int = 0;
		
		
		private var _image:Image = new Image(null);
		
		
		private var _frame:Image = new Image(null);
		private var _frameIndex:uint = 0;
		private var _frameTexture:Vector.<Texture> = new Vector.<Texture>();
		
		private var _streamRect:Rectangle = new Rectangle(0,0,0,0);   			// Resolución de la webcam
		
		// Stream de la cámara escalado
		private var _streamScaledRect:Rectangle;								// Resolución de la webcam escalado

		
		// viewport de la cámara
		private var _viewportRect:Rectangle = new Rectangle(0,0,0,0);		// El viewport rect que queremos ver
		private var _viewportPosition:Point = new Point(0,0)					// posicion del viewport en la imagen de salida
			
		private var _scaleFactor:Number = 1;									//  Que tanto se escala la salida de la cámara en modo "showAll"

		

		public function CameraTexture()
		{
			
			super()//null);
			
			this.addChild(this._image);
			this.addChild(this._frame);
		}
		
		public function start(_cameraIndex:String, _frameTexture:Vector.<Texture> = null):void
		{
		
			this._frameTexture = _frameTexture;
			
			
			
			this._cameraIndex = _cameraIndex;
		
			// stream scaled
			this._streamScaledRect = RectangleUtil.fit(this._streamRect,this._viewportRect,"noBorder",false);
			this._scaleFactor = this._streamRect.width/this._streamScaledRect.width;

			if(this._streamScaledRect.x < 0 || this._streamScaledRect.y < 0)
			{
				// si son negativas quiere decir q es más grande q el viewport, debemos setear la cámara al tamaño del viewport
				this._streamRect.width = this._viewportRect.width;
				this._streamRect.height = this._viewportRect.height;
			}
					
			// obtener el stream de la camara
			this._camera = flash.media.Camera.getCamera(this._cameraIndex);
			this._camera.setMode(this._streamRect.width,this._streamRect.height,this._fps);
			
			this._camera.setQuality(this._bandwidth,this._quality);

			this._image.texture = Texture.fromCamera(this._camera,this._scaleFactor, function():void
			{
				_image.readjustSize()
				_image.x = _viewportPosition.x;
				_image.y = _viewportPosition.y;
				
				update();
			});
		}
		
		
		public function next():void
		{
			this._frameIndex = ++this._frameIndex>this._frameTexture.length-1?0:this._frameIndex;
			update();
		}
		
		public function back():void
		{
			this._frameIndex = --this._frameIndex<0?this._frameTexture.length-1:this._frameIndex;
			update();
		}
	
		private function update():void
		{
			if(this._frameTexture.length>0)
			{
				this._frame.texture = this._frameTexture[this._frameIndex];
				trace(this._frameIndex, this._frame.texture)
				this._frame.readjustSize();
			}
			
		}
		public function set streamRect(_streamRect:Rectangle):void
		{
			this._streamRect = _streamRect;
		}
		
		public function get streamRect():Rectangle
		{
			return this._streamRect;
		}
		
		public function set viewportRect(_viewportRect:Rectangle):void
		{
			this._viewportRect = _viewportRect;
		}
		
		public function get viewportRect():Rectangle
		{
			return this._viewportRect;
		}
		
		public function set viewportPosition(_viewportPosition:Point):void
		{
			this._viewportPosition = _viewportPosition;
		}
		
		public function get viewportPosition():Point
		{
			return this._viewportPosition;
		}
		
		public function set fps(_fps:Number):void
		{
			this._fps = _fps;
		}
		
		public function get fps():Number
		{
			return this._fps;
		}
		
		public function set bandwidth(_bandwidth:int):void
		{
			this._bandwidth = _bandwidth;
		}
		
		public function get bandwidth():int
		{
			return this._bandwidth;
		}
		
		public function set quality(_quality:int):void
		{
			this._quality = _quality;
		}
		
		public function get quality():int
		{
			return this._quality;
		}

		
		override public function dispose():void
		{
			trace(" [XXXXX DISPOSE XXXXX ] " )
			
			if(_image.texture)
			{
				_image.texture.root.attachCamera(null);
				_image.texture.dispose();
				_image.texture = null;
				
				
				// si pongo esto afuera hace crash al salr
				while(numChildren>0)
				{
					removeChildAt(0,true);
				}
				super.dispose();
			}
		}
	}
}