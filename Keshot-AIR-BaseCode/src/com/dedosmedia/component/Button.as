package com.dedosmedia.component
{
	import feathers.controls.Button;
	import starling.events.Event;
	public class Button extends feathers.controls.Button
	{
		
		private var _sound:String;
		
		public function Button()
		{
			super();
		}
		
		public function set sound(_sound:String):void
		{
			this._sound = _sound
		}
		
		public function get sound():String
		{
			return this._sound;
		}
		
		public function button_triggeredHandler(e:Event):void
		{
			trace("BUTTON TRIGGER");
		}
	}
}