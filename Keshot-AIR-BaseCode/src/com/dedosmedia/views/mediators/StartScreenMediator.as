package com.dedosmedia.views.mediators
{
import com.dedosmedia.events.NativeProcessEventType;
import com.dedosmedia.events.SoundEventType;
import com.dedosmedia.views.components.StartScreen;
	
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	
	import feathers.controls.Button;
	
	import starling.animation.DelayedCall;
	import starling.core.Starling;
	import starling.events.Event;
import starling.utils.StringUtil;

public class StartScreenMediator extends CustomMediator
	{

		
		private var delayedCall:DelayedCall
		private var counter:uint = 0;

		
		override public function onRegister():void
		{
			trace("onRegister StartScreenMediator ")

			super.onRegister();		
			
			Starling.current.skipUnchangedFrames = true;

			//this.addContextListener(NativeProcessEventType.NATIVEPROCESS_COMPLETED,nativeProcessCompleted_handler,Event);
            //this.addContextListener(NativeProcessEventType.NATIVEPROCESS_ERROR,nativeProcessError_handler,Event);

		}


        private function nativeProcessError_handler(event:Event):void {
			trace("ERROR:::: XXXXXX RENDERIZANDO");
        }

		private function nativeProcessCompleted_handler(event:Event):void {

            trace("PROCESO COMPLETO!! SEND ANE HELPER", event.data)
            trace(event.data.input)



			// Mueve los archivos de salida a la carpeta temp
			for(var i:Number = 0, l:Number = appModel.outputFrames; i<l; i++)
			{
                var temp_output:File = appModel.projectDirectory.resolvePath(StringUtil.format("master/output{0}.{1}",i,appModel.fileExtension));
                if (temp_output.exists) {
                    var output:File = appModel.input.parent.parent.resolvePath(StringUtil.format("output/output{0}.{1}",i,appModel.fileExtension));

					trace("copy from: ",temp_output.nativePath,"to ",output.nativePath)
					trace("existen: ", temp_output.exists, output.exists)
                    temp_output.copyTo(output, true);
                }
                else
                {
                    trace("ERROR no existe imagen de salida. Existe la carpeta output en after?")
                    output = appModel.projectDirectory.resolvePath("master/input/input0.jpg");
                }
			}







            appModel.localConnection.send("localhost","echo",{"output":output.nativePath});
			appModel.localConnection.send("localhost","receiveMessage",{"output":output.nativePath, "outputFrames":appModel.outputFrames});


        }


		
		override protected function button_triggeredHandler(e:Event):void
		{
			super.button_triggeredHandler(e);
			
			switch(Button(e.currentTarget).name)
			{
				case "home-button":

					break;
			}
		}
		override public function onRemove():void
		{
			trace("onRemove::StartScreenMediator")
			super.onRemove();
		}

	}
}