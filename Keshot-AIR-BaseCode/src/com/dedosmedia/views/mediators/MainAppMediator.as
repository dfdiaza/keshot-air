package com.dedosmedia.views.mediators
{

import com.dedosmedia.events.ErrorEventType;
import com.dedosmedia.events.FileEventType;
import com.dedosmedia.events.NativeProcessEventType;
import com.dedosmedia.events.SoundEventType;
import com.dedosmedia.model.AppModel;
import com.dedosmedia.model.AssetsModel;
import com.dedosmedia.utils.ArrayUtil;
import com.dedosmedia.utils.TwoWayLocalConnection;
import com.dedosmedia.views.components.StartScreen;
import com.tuarua.WindowsHelperEvent;
import com.tuarua.fre.ANEError;


import feathers.controls.ScrollPolicy;

import flash.desktop.NativeApplication;
import flash.display.Bitmap;
import flash.display.Loader;
import flash.display.Stage;
import flash.display.StageDisplayState;
import flash.errors.IOError;
//import flash.events.Event;
import flash.events.FileListEvent;
import flash.events.IOErrorEvent;
import flash.events.InvokeEvent;
import flash.filesystem.File;
import flash.filesystem.FileMode;
import flash.filesystem.FileStream;
import flash.geom.Rectangle;
import flash.net.URLLoader;
import flash.net.URLRequest;
import flash.system.ApplicationDomain;
import flash.system.LoaderContext;
import flash.text.Font;

import flash.ui.Keyboard;
import flash.utils.ByteArray;

import adobe.utils.ProductManager;

import feathers.controls.Alert;
import feathers.controls.ImageLoader;
import feathers.controls.List;
import feathers.controls.StackScreenNavigatorItem;
import feathers.controls.renderers.DefaultListItemRenderer;
import feathers.controls.renderers.IListItemRenderer;
import feathers.data.ListCollection;
import feathers.motion.Slide;



import org.robotlegs.starling.mvcs.Mediator;

import starling.core.Starling;

import starling.text.TextField;
import starling.core.Starling;
import starling.events.Event;
import starling.events.KeyboardEvent;
import starling.text.TextField;
import starling.text.TextFormat;
import starling.textures.Texture;
import starling.utils.RectangleUtil;
import starling.utils.ScaleMode;
import starling.utils.StringUtil;
import starling.utils.StringUtil;
import starling.utils.SystemUtil;


import com.tuarua.WindowsHelperANE;


public class MainAppMediator extends Mediator
{

    public var screen:MainApp


    [Inject]
    public var appModel:AppModel;

    [Inject]
    public var assetsModel:AssetsModel;



    private var isPurging:Boolean = false;


    private var availableProjects:ListCollection;
    private var projects:List;


    private var loader:Loader;


    private var _initialized:Boolean = false;


    ///---- NEW

    private var availableAspectRatio:Array = new Array();		// Lista de carpetas de contenido con los aspect ratios disponibles
    private var availableScale:Array = new Array();				// Lista de carpetas de contenido con las resoluciones disponibles dentro determinado aspect ratio
    private var factor: Number = 1;								// Factor de escala a usar para el AssetManager


    private var currentAspectRatio:Number;						// Relación de aspecto actual de nuestra pantalla
    private var assetsPath:File;								// Apunta a la carpeta donde estan los assets segun el mejor aspect ratio y resolución para la pantalla actual
    private var assetsConfig:Object								// Contiene el json de configuracion generico de las texturas (indica a q resolucion se diseñó la interfaz)

    private var ane:WindowsHelperANE;

    private var keshot_file:File;

    private var _dataObject:Object


    private var AEwindowTitle:String;
    private var foundWindowTitle:String;

    private var retries:uint = 0;

    private var settings:Object

    private var copy_error:Boolean = false;

    //temp/session
    private var sessionFolder:File;

    override public function onRegister():void
    {
        super.onRegister();

        screen = this.getViewComponent() as MainApp;

        appModel.localConnection = new TwoWayLocalConnection("AIR","SWF", this)

        NativeApplication.nativeApplication.addEventListener(InvokeEvent.INVOKE, onInvoke);

        trace("BEGIN ANE INITIALIZING");

        ane = new WindowsHelperANE();
        ane.addEventListener(WindowsHelperEvent.UPLOAD_COMPLETE,ane_uploadCompleteHandler)



        /*
         *  START
         */

        var startItem:StackScreenNavigatorItem = new StackScreenNavigatorItem( StartScreen );
        screen.addScreen( MainApp.START, startItem );

        screen.pushTransition = Slide.createSlideLeftTransition();
        screen.popTransition = Slide.createSlideDownTransition()

        this.addContextListener(ErrorEventType.ERROR, handlerError,starling.events.Event);
        this.addContextListener(NativeProcessEventType.NATIVEPROCESS_ERROR, nativeProcessError_handler)
        this.addContextListener(NativeProcessEventType.NATIVEPROCESS_COMPLETED, nativeProcessCompleted_handler)


        Starling.current.stage.addEventListener(KeyboardEvent.KEY_DOWN, onKey);

        appModel.os = SystemUtil.platform;

        var releaseNumber:Array = ["",".1",".2",".3",".4",".5"];
        var releaseYear:Array = ["2014","2015","2016","2017","2018","2019"];
        var version:Array = new Array();

        for(i = 0; i<releaseYear.length;i++)
        {
            for(var  j:uint = 0; j<releaseNumber.length;j++)
            {
                version.push(releaseYear[i]+releaseNumber[j])
            }
        }

        var daemon_path:String;
        var render_engine_path:String;
        switch(SystemUtil.platform)
        {
            case "WIN":
                daemon_path =  "C:\\Program Files\\Adobe\\Adobe After Effects CC {0}\\Support Files\\aerender.exe";
                render_engine_path =  "AfterFX.exe";
                break;
            case "MAC":
                daemon_path =  "/Applications/Adobe After Effects CC {0}/aerender"
                render_engine_path =  "Adobe After Effects Render Engine.app"
                break;
        }


        var aerender:File;
        for(var i:uint =0, l:uint = version.length; i < l; i++)
        {
            aerender =  File.desktopDirectory.resolvePath(StringUtil.format(daemon_path,version[i]));
            if(aerender.exists)
            {
                appModel.aerender = aerender;
                trace("Existe una versión de aerender, en esta ruta: ",aerender.url);
                var afterfx:File = aerender.parent.resolvePath(render_engine_path);
                trace("AfterFX: ",afterfx.url, "Existe?",afterfx.exists);
                appModel.afterfx =  afterfx;
                break;
            }
        }

        if(!aerender.exists)
        {
            throw new Error("No se encontró una versión de After Effects válida. Debes instalar una versión de After Effects Creative Cloud");
            return;
        }

        showProjects();
        AWS();
    }

    private function ane_uploadCompleteHandler(event:WindowsHelperEvent):void {
        trace(" UPLOADING NEXT PICTURE");
        startUploading();
    }

    private function AWS():void
    {
        trace("Trying to initialize AWS SDK");
        ane.AWS("AKIAIZFO43UDMTOTETYQ", "g4SMKxtGCFP29ft+pjevTT4FuW7nB3MPsoy5QeWg");
    }

    function bringKeshotToFront():void
    {
        var windowName:String = "Keshot GUI";
        var window = ane.findWindowByTitle(windowName);
        trace("ENCONTRÓ KESHOT? "+windowName+" ?"+window);
        var result:Boolean = ane.makeTopMostWindow();
        trace("BRING TO FRONT? "+result);

        /*
        windowName = "Shell_TrayWnd";
        window = ane.findTaskBar();
        //ane.makeBottomWindow();
        var result:Boolean = ane.makeBottomWindow();
        trace("ENCONTRÓ "+windowName+" ?"+window+" == "+result);
        */

    }

    function minimizeAE():void
    {
        var windowName:String = AEwindowTitle;
        var window:String = ane.findWindowByTitle(windowName);
        trace("ENCONTRÓ AE"+windowName+" ?"+window);
        var done = ane.resizeWindow(50,50,0,0);
        trace("LO MINIMIZA? "+done);
    }

    private function onInvoke(event:InvokeEvent):void
    {
        trace("InvokeEvent - Invoke Directory", event.currentDirectory.url);
        trace("arguments ",event.arguments.length, event.arguments.join(","), arguments.length, arguments.join("-"));

        if(event.arguments.length > 0) {
            if (event.arguments[0] == "AFTERFX_LAUNCHED") {

                // Si settings es null, DedosMedia está siendo lanzado por AE...
                if(!settings)
                {
                    trace("Se lanza AE solo, no desde Dedos. Cerrar DedosMedia");
                    NativeApplication.nativeApplication.exit(1);
                    return;
                }




                minimizeAE();
                launchKeshot();


            }
        }

    }



    private function launchKeshot():void
    {
        if(settings.keshot && settings.keshot.launchOnStart == true)
        {
            // determinar si Kiosk.exe está corriendo, si no, lanzarlo
            var exe:File = new File(settings.keshot.executable);

            var isRunning:Boolean = ane.isProgramRunning(getFileName(exe.name));

            if(isRunning){
                trace("Kiosk.exe is already running");
            }
            else
            {
                trace("Kiosk.exe is not running yet ****");

                var keshot:String = settings.keshot.executable || "C:\\Keshot2014\\Kiosk.exe";
                var file:File = new File(keshot);

                trace("KIOSK: "+file.nativePath+" INI: "+settings.keshot.ini);

                //read Ini values to know which project to launch
                appModel.config.kioskCode = ane.readIniValue("Main","KioskCode", settings.keshot.ini);
                appModel.config.locationCode = ane.readIniValue("Main","LocationCode", settings.keshot.ini);
                appModel.config.GUIFolder = ane.readIniValue("Main","GUIFolder", settings.keshot.ini);


                // Crear carpeta temp/session, requerida para el flashcapture guardar las tomas de la sessión
                sessionFolder = File.applicationStorageDirectory.resolvePath("session");
                try
                {
                    sessionFolder.createDirectory();
                    sessionFolder.resolvePath("upload").createDirectory();
                }
                catch (e:Error)
                {
                    trace("ERROR CREANDO DIRECTORIO session/uplosd",e.getStackTrace());
                }

                trace('Directorio session creado');



                trace(appModel.config.kioskCode, appModel.config.locationCode, appModel.config.GUIFolder);

                var kiosk:File = new File(appModel.config.GUIFolder).resolvePath("Kiosk.exe");

                trace("New Kiosk exist? ",kiosk.exists, file.exists);
                try {
                    kiosk.copyTo(file,true);
                }
                catch (e:Error)
                {
                    trace("ERROR COPYING Kiosk.exe from event to Keshot2014 folder ", e.getStackTrace());
                }



                if(file.exists)
                {
                    // captive da algun problema?
                    file.openWithDefaultApplication();
                }
                else
                {
                    trace("Kiosk.exe no existe en la ubicación configurada");
                }

                startUploading();
            }

        }

        // No importa si estaba o no cargado, hay que pasarlo a primer plano
        Starling.juggler.delayCall(bringKeshotToFront,1);
    }

    private function backupAE():void
    {
        if(settings.backup && settings.backup.restoreBackupOnStart == true)
        {
            if(!settings.backup.backupDestination)
            {
                throw new Error("No hay configurado un destino para restaurar el backup")
            }
            if(!settings.backup.backupSource && settings.backup.backupSource == "")
            {
                throw new Error("No hay configurado un source del backup a restaurar")
                return;
            }

            var src_path:String = settings.backup.backupSource;
            var src:File = File.applicationStorageDirectory.resolvePath(src_path);

            var appDataFile:File = new File(File.applicationStorageDirectory.nativePath).parent.parent
            var backupDestination:String = StringUtil.format(settings.backup.backupDestination,appDataFile.nativePath);

            trace("BackupDestination ",backupDestination)

            var dst:File = new File(backupDestination).resolvePath(src.name);

            trace(src.nativePath, src.exists, src.name);
            trace(dst.nativePath, dst.exists, dst.name);

            if(src.exists && dst.exists)
            {
                trace("COPIAR BACKUP")
                copyFolder(src,dst);
            }
            else
            {
                trace("NO EXISTE CARPETA DE BACKUP PARA RESTAURAR, O DESTINO");
            }

        }
    }

    private function launchAE():void
    {

        // Revisar si debemos hacer el backup al empezar
        backupAE();

        if(settings.render && settings.render.launchOnStart == true)
        {
            var isRunning:Boolean = ane.isProgramRunning(getFileName(appModel.afterfx.name));
            if(isRunning){
                trace(appModel.afterfx.name+" is running... try to launch keshot");
                minimizeAE();
                launchKeshot();
            }
            else
            {
                trace("AfterFX is not running yet ");
                var render:String;
                if(settings.render && settings.render.executable && settings.render.executable != "")
                {
                    render = settings.render.executable;
                }
                else
                {
                    render = appModel.afterfx.nativePath;
                }

                trace("Launch AfterFX "+render)

                var file:File = new File(render);
                if(file.exists)
                {
                    file.openWithDefaultApplication();
                }
                else
                {
                    trace("AfterFx.exe no existe en la ubicación configurada");
                }

            }

        }
    }



    private function purgeCacheAE():void {

        var obj:Object = new Object();


        isPurging = true;

        //app.executeCommand(10200);
        //alert("Cache cleared!");
        //All Memory & Disk Cache... = 10200
        //All Memory = 2373
        //Undo = 2371
        //Image Cache Memory = 2372
        //Snapshot = 2481



        obj.purgeMemory = true;
        obj.executable = appModel.afterfx;
        var command:String = "app.purge(PurgeTarget.UNDO_CACHES); app.purge(PurgeTarget.SNAPSHOT_CACHES); app.purge(PurgeTarget.IMAGE_CACHES); app.purge(PurgeTarget.ALL_CACHES);";
        var processArgs:Vector.<String> = new Vector.<String>();
        processArgs.push("-s");  //-so
        processArgs.push( command );
        obj.processArgs = processArgs;

        trace("Argumentos para AfterFx ",processArgs)


        this.dispatchWith(NativeProcessEventType.NATIVEPROCESS_START,false,obj);
    }

    private function runRenderScript():void {

        var obj:Object = new Object();

        obj.purgeMemory = true;
        obj.executable = appModel.afterfx;



        var script:String = appModel.projectDirectory.resolvePath("master/run.js").nativePath;


        var processArgs:Vector.<String> = new Vector.<String>();
        processArgs.push("-r");
        processArgs.push( "\""+script+"\"" );

		obj.processArgs = processArgs;

        trace("Argumentos para AfterFx runRenderScript2 ",processArgs)
        this.dispatchWith(NativeProcessEventType.NATIVEPROCESS_START,false,obj);
    }




    private function nativeProcessError_handler(e:starling.events.Event):void
    {
        // Entra al if, cuando se manda comando de purge llamando a afterfx.exe en lugar de AfterFx.com (q no genera process error)
        if(isPurging)
        {
            isPurging = false;
            trace("MEMORY PURGED CORRECTLY");
            //setWindowsOrder();

        }
        else
        {
            trace("MainAppMediator:: onCmdError",e.data)

        }
    }

    private function nativeProcessCompleted_handler(e:starling.events.Event, data:Object):void
    {
        trace("Proceso completado. Era proceso de Purge memory? ",data.hasOwnProperty("purgeMemory"));



        /*
        var found = ane.findWindowByTitle(AEwindowTitle);
        trace("------------ "+AEwindowTitle+" (2) WINDOW WAS FOUND?",found);
        var isRunning:Boolean = ane.isProgramRunning(getFileName(appModel.afterfx.name));
        trace("------------ isRunning "+appModel.afterfx.name+"? ",isRunning);
        var done = ane.resizeWindow(50,50,0,0);
        var done = ane.makeBottomWindow();
        trace("RESIZED (2)?? ",done);
        */

        //setWindowsOrder();

        if(!data.hasOwnProperty("purgeMemory"))
        {
            //var input:File = new File(appModel.input);
            trace("input parent", appModel.input.parent.nativePath);
            trace("outputFrames ",appModel.outputFrames)

            trace("IS PURGING MEMORY? ", data.hasOwnProperty("purge"))

            var pictures:Array = new Array();
            // Mueve los archivos de salida a la carpeta temp
            for(var i:Number = 0, l:Number = appModel.outputFrames; i<l; i++)
            {
                var temp_output:File = appModel.projectDirectory.resolvePath(StringUtil.format("master/output{0}.{1}",i,appModel.fileExtension));
                if (temp_output.exists) {

                    var fileName:String = StringUtil.format("output{0}.{1}",i,appModel.fileExtension);

                    var output:File = appModel.input.parent.parent.resolvePath(fileName);
                    temp_output.copyTo(output, true);

                }
                else
                {
                    trace("ERROR no existe imagen de salida. Existe la carpeta output en after?")
                    output = appModel.projectDirectory.resolvePath("master/input/input0.jpg");
                }

                pictures.push(output.nativePath);
            }

            /*
            trace("RESIZE");
            var outputString = ane.resizeImage(pictures, 250,250);
            trace("OUTPUT ALGO",outputString);
            */


            // quitar el foco a After effects y limpiar cache
            //lookForAE();
            purgeCacheAE();



            appModel.localConnection.send("localhost","echo",{"output":output.nativePath});
            appModel.localConnection.send("localhost","receiveMessage",{"output":output.nativePath});
        }
        else
        {
			trace("Memoria Borrada Correctamente");


        }



    }

    public function echo(obj:Object):void {
        trace("RECIBO ECHO");
        for(var i in obj)
            trace(i,obj[i])
    }





    private function resolveToBasePath(item:String,index:int,arr:Array):String
    {
        var f:File =  appModel.basePath.resolvePath(item);
        trace(appModel.basePath.resolvePath, "F: ",f.exists,f.nativePath);
        return f.nativePath;

    }

    // Tenbemos las tomas de salida de la session completas.  Debemos redimensionar y guardar en carpeta de Session
    public function endSession(obj:Object):void {

        trace(" > END SESSION ");
        trace("RECIBO");
        for (var i in obj)
            trace(i, obj[i])

        var filesPath:Array = obj.files.map(resolveToBasePath);
        trace("files ",filesPath);



        //guardar las imágenes de entrada en una carpeta photos/{sessioncode}/*
        try
        {
            var inputFilesPath:Array = obj.input;
            var photos:File = appModel.basePath.resolvePath("photos/"+obj.session);

            photos.createDirectory();
            for(var n=0; n<inputFilesPath.length;n++)
            {
                var input:File = photos.resolvePath(inputFilesPath[n]);
                input.copyTo(photos.resolvePath("input/"+input.name),true);
            }

            for(n=0; n<filesPath.length;n++)
            {
                input = photos.resolvePath(filesPath[n]);
                input.copyTo(photos.resolvePath("output/"+input.name),true);
            }
        }
        catch(e:Error)
        {
            trace("ERRO CREANDO PHOTOS FOLDER");
        }




        // EL ANE está generando error en el image.Resize();
        var output = ane.resizeImage(filesPath, 250,250);
        trace("ResizeImage ",output);
        if(output == "")
        {
            trace("no se pudo redimensionar imagen");
        }
        else
        {
            trace("output GIF : ", output);
            //mover gif a otra carpeta con el nombre de la session y crear un json con datos respectivos

            var outputFile:File = new File(output);
            if(!outputFile.exists)
            {

                trace("WRONG OUTPUT GIF");
                return;
            }


            var dstFile:File = sessionFolder.resolvePath("upload/"+obj.session+".gif");
            outputFile.copyTo(dstFile,true);

            var data:Object = new Object();
            data.file = dstFile.name;
            data.metadata = new Object();
            data.metadata["location-code"] = appModel.config.locationCode;
            data.metadata["kiosk-code"] = appModel.config.kioskCode;
            data.metadata["session-code"] = obj.session;
            data.metadata["epoch"] = String(Math.round(new Date().time/1000));


            var dataAsString:String = JSON.stringify(data);
            var dataFile:File = dstFile.parent.resolvePath(obj.session+".json");
            trace("Data File",dataFile.nativePath);

            try
            {
                var fileStream:FileStream = new FileStream();
                fileStream.open(dataFile, FileMode.WRITE);
                fileStream.writeUTFBytes(dataAsString);
                fileStream.close();
            }
            catch(e:Error)
            {
                trace("XXX ERROR: ALGUN ERROR CREANDO FICHERO "+obj.session+".json",e.getStackTrace());
            }
        }

        startUploading();

    }

    private function isJSON(item:File, idx:uint, arr:Array):Boolean {

        return (item.extension.toLowerCase() == "json" && !item.isDirectory);
    }


    // Inicia la subida de un archivo asociado a un JSON
    private function startUploading():void
    {
        var sessionFiles:Array = sessionFolder.resolvePath("upload").getDirectoryListing();
        var jsonFiles = sessionFiles.filter(isJSON);
        if(jsonFiles.length == 0)
        {
            trace("ALL JSON FILES ALREADY UPLOADED.")
            return;
        }

        jsonFiles.sort(ArrayUtil.randomSort);
        trace("JSON FILES PENDING: ",jsonFiles.length)

        var jsonFile:File = jsonFiles[0];
        if(jsonFile && jsonFile.exists)
        {

            // READ JSON FILE
            var stream:FileStream = new FileStream();
            stream.open(jsonFile, FileMode.READ);
            var json:Object = JSON.parse(stream.readUTFBytes(stream.bytesAvailable));
            stream.close();


            //var jsonDirectory:File = jsonFile.parent;
            var pictureFile:File = jsonFile.parent.resolvePath(json.file)
            if(pictureFile.exists)
            {
                try {
                    var bucket = "keshot-dedosmedia";
                    var uploadResult = ane.uploadFile(jsonFile.nativePath, pictureFile.nativePath, bucket, json.metadata);
                    trace("> UPLOAD RESULT: ",uploadResult);
                } catch (e:ANEError) {
                    trace("e.message: ", e.message);
                    trace("e.type: ", e.type);
                    trace("e.errorID", e.errorID);
                    trace("e.getStackTrace", e.getStackTrace());
                }
            }
            else
            {
                trace("No hay imagen ",pictureFile.nativePath, "como se indica en",jsonFile.nativePath);
            }
        }
        else
        {
            trace("No hay fichero JSON",jsonFile.nativePath);
        }

    }



    public function update(obj:Object):void {
        trace("RECIBO UPDATE");
        // En esta función se debe recibir el path a la carpeta de filtros y pasarlo a la app de AIR
        for(var i in obj)
            trace(i,obj[i])

        if(!appModel.contentUpdated)
        {
            appModel.contentUpdated = true;

            var filters:File = new File(obj.filters);

        }

    }

    private function copyFolder(directoryToCopy:File, locationCopyingTo:File):void
    {
        var directory:Array = directoryToCopy.getDirectoryListing();

        for each (var f:File in directory)
        {
            if (f.isDirectory)
                copyFolder(f, locationCopyingTo.resolvePath(f.name));
            else
                f.copyTo(locationCopyingTo.resolvePath(f.name), true);
        }
    }

    private function copyInto(directoryToCopy:File, locationCopyingTo:File):void
    {
        trace("CopyInto from source",directoryToCopy.nativePath," to destine",locationCopyingTo.nativePath);
        var directory:Array = directoryToCopy.getDirectoryListing();
        for each (var f:File in directory)
        {
            trace("We are copying this file ", f.nativePath)
            if (f.isDirectory)
            {
                trace(" -- This is a directory.")
                copyInto(f, locationCopyingTo.resolvePath(f.name));
            }
            else
            {
                var dstFile:File = locationCopyingTo.resolvePath(f.name);
                trace(" -- This is a file. Our new destination is:",dstFile.nativePath)
                try
                {
                    f.copyTo(dstFile,true);
                }
                catch (error:IOError)
                {
                    trace("ERROR:  NO SE PUDO COPIAR LA EL ARCHIVO EN PRIMER INTENTO.",error.getStackTrace())
                    trace("EL PROPBLEMA LO GENERA: ");
                    try
                    {
                        dstFile.deleteFile();
                    }
                    catch (error:Error)
                    {
                        trace("Archivo destino no se deja borrar",dstFile.nativePath);
                        copy_error = true;
                    }

                    this.dispatchWith(SoundEventType.PLAY_SOUND,true, "error")



                }
                catch (error:Error)
                {
                    trace("ERROR: XXXXX NO SE PUDO COPIAR GENERICO ",error.getStackTrace())
                }

            }
        }
    }


    public function receiveMessage(obj:Object):void {

        try{
            this._dataObject = obj;


            trace("Mensaje recibido desde Flash: ");
            for(var i in obj)
                trace(i," - ",obj[i]);

            // guardar la data en un archivo C:\keshot2014/data.js
            if(obj.data)
            {
                var text:String = "var data = ";
                text += JSON.stringify(obj.data);
                trace("TEXT ",text);
                this.dispatchWith(FileEventType.FILE_SAVE_START,false,{text:text, file:new File("C:\\Keshot2014\\data.js"), mode:FileMode.WRITE, type:"text"})
            }



            appModel.basePath = new File(obj.projectPath).parent;

            var dllName:String = getFileName(obj.aepx);
            trace("*** dllName ",dllName);
            // NO PUEDE IR ATADO AL NOMBRE DE LA CARPETA... la renomrba mucho



            // Copiamos el .dll zipped desde el proyecto de FLash al de AIR

            var zippedSource:File = appModel.basePath.resolvePath(StringUtil.format("content/filter/{0}.dll",dllName));
            var zipFile:File = appModel.projectDirectory.resolvePath("master/master.dll");


            try {
                 zipFile.deleteFile();
            }
            catch (e:Error)
            {
                trace("1 ERROR BORRANDO ARCHIVO... QUIZAS NO EXISTE? ", zipFile.exists);
            }

            trace("zippedSource ", zippedSource.nativePath, zipFile.nativePath)
            zippedSource.copyTo( zipFile);


            // Descompriomir el ZIP
            var unzipped = ane.unzipFile( zipFile.nativePath,  zipFile.parent.nativePath, dllName)

            if(!unzipped)
            {

                trace("ERROR: LA CONTRASEÑA NO ES CORRECTA");
            }




            appModel.input = new File(obj.input);


            var inputSource:File = appModel.input.parent;
            var inputDst:File = appModel.projectDirectory.resolvePath("master/input");

            trace("****** Las fotos de entrada están guardadas aquí: ",inputSource.nativePath, "y se van a mover aquí: ",  inputDst.nativePath);
            trace(inputSource.exists,  inputDst.exists);
            copyInto(inputSource, inputDst);


			processRender();
            


        }
        catch (e:Error)
        {
            // Si hay una excepcion... parece q no se ejecutara la llamada al a funcion
            trace("CATCH ERROR",e.getStackTrace());
        }

    }


    private function processRender():void {

        trace("PROCESS THE RENDER SCENE")
        var obj:Object = this._dataObject;
        obj.executable = appModel.aerender;

        trace("Executable",obj.executable.nativePath);



        var processArgs:Vector.<String> = new Vector.<String>();
        processArgs.push("-project");

        var aepx:String = appModel.projectDirectory.resolvePath("master/"+obj.aepx).nativePath;
        trace("******************** aepx",aepx)
        processArgs.push( aepx );

        processArgs.push("-comp");
        processArgs.push(obj.composition);
        trace("composition",obj.composition);

        appModel.fileExtension = obj.outputModule;

        processArgs.push("-OMtemplate");
        processArgs.push(obj.outputModule);

        var output:File = appModel.projectDirectory.resolvePath(StringUtil.format("master/output[%23].{0}",obj.outputModule));
        processArgs.push("-output");
        processArgs.push(output.nativePath);
        trace("output ",output.nativePath)

        //var output_dir:File = new File(output.url.substring(0,output.url.lastIndexOf("/")))
        //output_dir.createDirectory();
        trace("Obj.start ",obj.start)
        if(obj.start != null)
        {
            processArgs.push("-s");
            processArgs.push(obj.start);
        }

        trace("Obj.end",obj.end)
        if(obj.end != null)
        {
            processArgs.push("-e");
            processArgs.push(obj.end);
        }
        trace("obj.increment",obj.increment)
        if(obj.increment != null)
        {
            processArgs.push("-i");
            processArgs.push(obj.increment);
        }

        appModel.outputFrames = Math.ceil( ((obj.end-obj.start)+1)/obj.increment);
        trace("outPut Frames ",appModel.outputFrames);


        //processArgs.push("-v");
        //processArgs.push("ERRORS");

        //processArgs.push("-v");
        //processArgs.push("ERRORS");


        //attributos desde XML
        //lanzar AE solo si activado


        processArgs.push("-close");
        processArgs.push("DO_NOT_SAVE_CHANGES");
        processArgs.push("-reuse");
        processArgs.push("-continueOnMissingFootage");

        obj.processArgs = processArgs;

        trace("args ",processArgs)

        isPurging = false;

        bringKeshotToFront();
        minimizeAE();
        this.dispatchWith(NativeProcessEventType.NATIVEPROCESS_START,false,obj)
    }




    private function onRequestResult($state:int):void
    {
        trace("permission result = " + $state);
        // Determinar si lanzar un proyecto definido o listar proyectos
        showProjects();
    }


	/*
	 * Muestra el listado de Proyectos disponibles para iniciar o lanza uno predefinido.
	 */
    private function showProjects():void
    {
        try
        {
            var projectName:String = screen.initConfig.config.settings[appModel.os].launchProject;
        }
        catch(e:Error)
        {
            projectName = "";
        }
		trace("appModel.os ",appModel.os, screen.initConfig.config.settings, screen.initConfig.config.settings[appModel.os].launchProject);
        var project:File = appModel.content.resolvePath(projectName);
		trace("ProjectFile: ",project.url, project.exists);
        if(project.resolvePath("json/config.json").exists)
        {
            trace("Lanzar proyecto predeterminado: ("+projectName+")");
            launchProject(project);
        }
        else
        {
            listAvailableProjects();
        }
    }


    //Muestra todos los errores en el software
    private function handlerError(e:starling.events.Event):void
    {
        Alert.show(e.data.text+" \n "+e.data.error,"Error",  new ListCollection(
                [
                    { label: "OK", triggered: function():void{trace("cerrar");} }
                ]));
    }

    private function onKey(event:KeyboardEvent, key:*):void
    {
        // flash native stage
        var _stage:Stage = Starling.current.nativeStage;
        if(event.ctrlKey)
        {
            switch(key)
            {
                case Keyboard.F:

                    if(_stage.displayState == StageDisplayState.NORMAL)
                    {
                        _stage.displayState = StageDisplayState.FULL_SCREEN_INTERACTIVE;
                    }
                    else
                    {
                        _stage.displayState = StageDisplayState.NORMAL;
                    }
                    break;
                case Keyboard.X:
                    Alert.show("¿Estás seguro que quieres cerrar la aplicación?","Confirmación",new ListCollection([{label:"Cerrar", triggered:closeHandler}, {label:"Cancelar"}]));
                    break;
                case Keyboard.R:
                    this.restart();
                    break;
            }
        }
    }

    private function restart():void {
        new ProductManager("airappinstaller").launch("-launch " + NativeApplication.nativeApplication.applicationID + " " + NativeApplication.nativeApplication.publisherID);
        NativeApplication.nativeApplication.exit();
    }

    private function closeHandler():void
    {
        Starling.current.nativeStage.nativeWindow.close();
    }



	/*
	 * Retorna un listado de carpetas, unicamente en las cuales exista el fichero /json/config.json
	 */
    private function isProjectFolder(item:Object, idx:uint, arr:Array):Boolean {
        return (new File(File(item).nativePath+"/json/config.json").exists);
    }

    private function listAvailableProjects():void
    {
        trace("Content directory",appModel.content.url," existe? "+appModel.content.exists)
        if(appModel.content.exists)
        {
            // Arreglo de File Objects
            var _allDirectory:Array = appModel.content.getDirectoryListing();
            var _onlyProjectDir:Array = _allDirectory.filter(isProjectFolder);

            trace("Hay "+_onlyProjectDir.length," carpetas de proyecto disponibles.")
            // al menos hay una carpeta de proyectos
            if(_onlyProjectDir.length > 0)
            {
                availableProjects = new ListCollection(_onlyProjectDir);
                projects = new List();
                projects.styleProvider = null;
                projects.horizontalScrollPolicy = ScrollPolicy.OFF;
                projects.itemRendererFactory = function():IListItemRenderer
                {
                    var renderer:DefaultListItemRenderer = new DefaultListItemRenderer();
                    renderer.labelField = "name";
                    return renderer;
                }

                projects.addEventListener(starling.events.Event.CHANGE,onProjectChosen);
                projects.width = Starling.current.viewPort.width;
                projects.height = Starling.current.viewPort.height;
                projects.dataProvider = availableProjects;
                screen.addChild(projects);
                return;
            }
        }

        try
        {
            appModel.content.createDirectory();
        }
        catch(e:Error)
        {
            throw new Error("No hay permisos de escritura en el disco para crear carpeta content.");
        }

        Alert.show("No hay ningún proyecto disponible.","Error",new ListCollection([{label:"Cerrar",triggered:closeHandler}]));
    }

    private function onProjectChosen(event:starling.events.Event):void
    {
        var list:List = List( event.currentTarget );
        trace(" Se ha seleccionado el siguiente proyecto",(list.selectedItem as File).url);
        launchProject(list.selectedItem as File);
    }


    private function onParseError(e:starling.events.Event):void
    {
        trace("XXXXXXXXXXXXXXXXX  ERROR MAL CONFIGURACION DE FICHERO JSON... NO SE PUEDE DECODIFICAR.  XXXXXXXXXXXXXX - ", e.data)
    }

    private function launchProject(file:File):void
    {
        appModel.projectDirectory = file;

        assetsModel.assets.scaleFactor = Starling.contentScaleFactor;
        assetsModel.assets.enqueue(appModel.projectDirectory.resolvePath("json"));
        assetsModel.assets.enqueue(appModel.projectDirectory.resolvePath("audio"));
        assetsModel.assets.addEventListener(starling.events.Event.PARSE_ERROR, onParseError);

        if(projects)
        {
            this.projects.removeFromParent(true);
        }

        var filePath:String = "loading.png";

        var loading:File = File.applicationDirectory.resolvePath(filePath);
        if(loading.exists)
        {
            var bytes:ByteArray = new ByteArray();
            var stream:FileStream = new FileStream();
            stream.open(loading, FileMode.READ);
            stream.readBytes(bytes, 0, stream.bytesAvailable);
            stream.close();
            this.loader = new Loader();

            this.loader.contentLoaderInfo.addEventListener("complete", function(e:*):void {
                trace("******  Loading");

                var img:ImageLoader = new ImageLoader();
                img.source = Texture.fromBitmap( loader.content as Bitmap );
                img.validate();
                screen.addChild(img);

                // Posicionar imagen en el centro a un 20% del tamaño del stage
                var screenSize:Rectangle = new Rectangle(0, 0, Starling.current.stage.width*0.2, Starling.current.stage.height*0.2);
                var rect:Rectangle = RectangleUtil.fit( (loader.content as Bitmap).bitmapData.rect, screenSize, ScaleMode.SHOW_ALL, true);
                img.x = (Starling.current.stage.width - rect.width)/2;
                img.y = (Starling.current.stage.height - rect.height)/2;
                img.width = rect.width;
                img.height = rect.height;

                loadFonts();
            });

            this.loader.loadBytes(bytes);
        }
        else
        {
            trace("NOT LOADING.PNG FOUND");
            assetsModel.assets.loadQueue(queueProgress);
        }


    }

    private function loadFonts():void
    {

        var fonts:File = appModel.projectDirectory.resolvePath("fonts/EmbeddedFonts.swf");
        if(fonts.exists)
        {
            var bytes:ByteArray = new ByteArray();
            var stream:FileStream = new FileStream();
            stream.open(fonts, FileMode.READ);
            stream.readBytes(bytes, 0, stream.bytesAvailable);
            stream.close();
            var loaderContext:LoaderContext = new LoaderContext();
            loaderContext.allowCodeImport = true;
            loaderContext.applicationDomain =  new ApplicationDomain();
            this.loader = new Loader();
            this.loader.contentLoaderInfo.addEventListener("complete", function(e:*):void {
                trace(" *Loaded fonts");
                var definitions:Vector.<String> = e.target.applicationDomain.getQualifiedDefinitionNames();
                for(var i:uint = 0, l:uint = definitions.length; i<l; i++)
                {
                    var FontLibrary:Class = e.target.applicationDomain.getDefinition(definitions[i]) as Class;
                    Font.registerFont(FontLibrary);
                }
                assetsModel.assets.loadQueue(queueProgress);
            });
            loader.loadBytes(bytes,loaderContext);
        }
    }

    private function queueProgress(ratio:Number):void
    {
        if (ratio == 1.0)
        {
            currentAspectRatio = Starling.current.nativeStage.stage.stageWidth / Starling.current.nativeStage.stage.stageHeight;
            trace("Current Aspect Ration: "+currentAspectRatio)

            // Buscar la carpeta con el aspect ratio mas parecido a nuestro actual
            var directory:File =  appModel.projectDirectory.resolvePath("textures");
            directory.getDirectoryListingAsync();
            directory.addEventListener(FileListEvent.DIRECTORY_LISTING, dirListHandler);
            directory.addEventListener(IOErrorEvent.IO_ERROR, dirListHandler_IO_ERROR);


        }
    }

    private function dirListHandler_IO_ERROR(event:FileListEvent):void
    {
        trace("Error reading the disk")

    }

    private function dirListHandler(event:FileListEvent):void
    {
        var folder:Array = event.files.filter(justFolderWithConfig)

        // crea un objeto con los aspect ratios disponibles
        for (var i:uint = 0; i < folder.length; i++)
        {
            var temp	:Array 		= folder[i].name.split("x");
            var w		:Number 	= Number(temp[0])
            var h		:Number 	= Number(temp[1])
            var ratio	:Number	 	= w/h;
            // solo folder cuyo nombre cumpla la regla wxh en su nombre (donde w y h deben ser numeros enteros)
            if(!isNaN(ratio))
            {
                availableAspectRatio.push({file:folder[i], ratio:w/h})
            }
        }

        //buscar el folder con la relacion de aspecto màs cercana a la que tiene nuestra máquina
        var index: uint = nearestNumber(currentAspectRatio,availableAspectRatio)
        assetsPath = availableAspectRatio[index].file

        trace("--Path for current Aspect Ratio: "+assetsPath.nativePath)



        //leer el archivo de config.json base para este aspect ratio (indica el tamaño del stage para starling)
        var file:File = assetsPath.resolvePath("./config.json");
        if(file.exists)
        {
            var request:URLRequest = new URLRequest(file.url);
            var configLoader:URLLoader = new URLLoader(request);
            configLoader.addEventListener(flash.events.Event.COMPLETE, function(event:flash.events.Event):void
            {
                try
                {
                    assetsConfig = JSON.parse(configLoader.data);


                    trace("-- Resolución del diseño: "+assetsConfig["design_size"].w,assetsConfig["design_size"].h);
                    appModel.design_size = new Rectangle(0,0,assetsConfig["design_size"].w,assetsConfig["design_size"].h);
                    initStarling();
                }
                catch(e:SyntaxError)
                {
                    trace("ERROR: Syntax error. JSON")
                }

            });
        }
        else
        {
            trace("ERROR: Config.json missing inside aspect ratio folder")
        }
    }

    // ontiene el folder con la resolucion mas cercana a la del equipo
    public function nearestNumber(value:Number,list:Array):uint {
        var currentNumber:Number = list[0].ratio;
        var index:uint = 0;
        for (var i:int = 0; i < list.length; i++) {
            if (Math.abs(value - list[i].ratio) < Math.abs(value - currentNumber)){
                currentNumber = list[i].ratio;
                index = i;
            }
        }
        return index;
    }


    private function initStarling():void
    {
        trace("Starling created: ")

        var stageSize:Rectangle  = new Rectangle(0, 0, Starling.current.stage.stageWidth, Starling.current.stage.stageHeight)	//  Que sea la mejor resolución escogida
        var screenSize:Rectangle = new Rectangle(0, 0, Starling.current.stage.stageWidth, Starling.current.stage.stageHeight); 	//  la resolución de nuestra pantalla
        var viewPort:Rectangle = RectangleUtil.fit(stageSize, screenSize, ScaleMode.SHOW_ALL);

        trace("NEW VIEWPORT ",viewPort)

        Starling.current.viewPort = viewPort;

        Starling.current.antiAliasing = assetsConfig.hasOwnProperty("antiAliasing")?assetsConfig["antiAliasing"].value:0;
        Starling.current.stage.stageWidth = Starling.current.stage.stageWidth;
        Starling.current.stage.stageHeight = Starling.current.stage.stageHeight;


        if(Starling.current.showStats == true)
        {
            Starling.current.showStatsAt("left","bottom",stageSize.width/screenSize.width);
        }


        trace("stageSize: "+stageSize)
        trace("screenSize: "+screenSize)
        trace("VP: "+viewPort)
        trace("Star Size "+Starling.current.stage.stageWidth,Starling.current.stage.stageHeight)
        trace("scale "+Starling.current.contentScaleFactor)

        assetsPath.getDirectoryListingAsync();
        assetsPath.addEventListener(FileListEvent.DIRECTORY_LISTING, assetsPathHandler);
        assetsPath.addEventListener(IOErrorEvent.IO_ERROR, assetsPath_IO_ERROR);
    }

    private function assetsPath_IO_ERROR(event:FileListEvent):void
    {
        trace("Error reading the disk")
    }

    private function readSize(path:File):Object{
        var file:File = path.resolvePath("./config.json")
        if(file.exists)
        {
            var stream:FileStream = new FileStream();
            stream.open(file, FileMode.READ);
            var json:Object = JSON.parse(stream.readUTFBytes(stream.bytesAvailable));
            stream.close();
            trace(json.size.w,json.size.h)
            return json.size;

        }
        else
        {
            trace("No hay fichero size.json inside ",file.nativePath)
            return 0;
        }
    }

    private function assetsPathHandler(event:FileListEvent):void
    {

        trace("events.files.filter ",event.files.filter)
        var folder:Array = event.files.filter(justFolderWithConfig)
        trace("folder",folder)

        // para el aspect ratio seleccionado... busca las carpetas y calcula la escala de cada una de ellas.
        for (var i:uint = 0; i < folder.length; i++)
        {

            // El ratio se calcula teniendo en cuenta unicamente el ancho
            var w:Number = readSize(folder[i]).w;
            var h:Number = readSize(folder[i]).h;
            var ratio:Number = w/Starling.current.stage.stageWidth; // assetsConfig["design_size"].w //
            availableScale.push({file:folder[i], ratio:ratio, w:w, h:h})

            trace("ESCALA:: "+ratio)
        }

        trace("available "+availableScale)
        // Buscar el zoom más cercano al contentScaleFactor
        var index: uint = nearestNumber(Starling.current.contentScaleFactor,availableScale)
        factor = availableScale[index].ratio;
        trace("-- AssetManager better scale: "+factor)
        trace("-- VP:"+Starling.current.viewPort,availableScale[index].w,availableScale[index].h)

        var stageSize:Rectangle  = new Rectangle(0, 0, availableScale[index].w, availableScale[index].h)	//  Que sea la mejor resolución escogida
        var screenSize:Rectangle = new Rectangle(0, 0, Starling.current.stage.stageWidth, Starling.current.stage.stageHeight); 	//  la resolución de nuestra pantalla



        var viewPort:Rectangle = RectangleUtil.fit(stageSize, screenSize, ScaleMode.SHOW_ALL);

        trace(stageSize,screenSize,viewPort)

        Starling.current.viewPort = viewPort;
        Starling.current.stage.stageWidth = viewPort.width;
        Starling.current.stage.stageHeight = viewPort.height;


        // Este es el factor real entre el diseño, y el tamaño en pixeles en q se ve el view port (redimensionado a ocupar la mayor parte de pantalla)
        factor = appModel.design_size.width/viewPort.width;
        trace("NEW FACTOR "+factor)
        trace("CSF: "+Starling.current.contentScaleFactor)


        assetsPath = availableScale[index].file;

        trace("-- Assets path "+assetsPath.nativePath)
        assetsModel.assets.scaleFactor = factor;
        assetsModel.assets.enqueue(assetsPath);
        assetsModel.assets.loadQueue(finalQueueProgress);

    }

    private function finalQueueProgress(ratio:Number):void
    {
        trace("**** Loading assets, progress:", ratio);
        if (ratio == 1.0)
        {
            trace("done ",this._initialized)


            while(screen.numChildren>0)
            {
                screen.removeChildAt(0,true);
            }

            screen.rootScreenID = MainApp.START;

             // Launch After



            settings = assetsModel.assets.getObject("settings");
            AEwindowTitle = settings.render.windowTitle;
            launchAE();




        }
    }



    // HELPERS



    // filtrar el array de archvios a solo los que son folder
    private function justFolderWithConfig(item:*, index:int, array:Array):Boolean {
        return File(item).isDirectory == true  && File(item).resolvePath("config.json").exists
    };

    private function getFileName(name:String):String
    {
        var extensionIndex:Number = name.lastIndexOf( '.' );
        return name.substr( 0, extensionIndex );
    }

}
}