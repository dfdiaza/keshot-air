package com.dedosmedia.views.components
{
	import feathers.controls.Screen;
	
	public dynamic class StartScreen extends Screen
	{
		public function StartScreen()
		{
			super();
		}
		
		override protected function initialize():void
		{
			super.initialize();
			trace("initialize::StartScreen.as");
		}
	}
}