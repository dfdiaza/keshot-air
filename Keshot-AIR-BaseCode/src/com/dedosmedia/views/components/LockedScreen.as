package com.dedosmedia.views.components
{
	import feathers.controls.Screen;
	
	public dynamic class LockedScreen extends Screen
	{
		public function LockedScreen()
		{
			super();
		}
		
		override protected function initialize():void
		{
			super.initialize();
			trace("initialize::LockedScreen.as");
		}
	}
}