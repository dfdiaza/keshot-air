package com.dedosmedia.views.components
{
	import feathers.controls.Screen;
	
	public dynamic class DisclaimerScreen extends Screen
	{
		public function DisclaimerScreen()
		{
			super();
		}
		
		override protected function initialize():void
		{
			super.initialize();
			trace("initialize::DisclaimerScreen.as");
		}
	}
}