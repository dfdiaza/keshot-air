package
{
	import com.dedosmedia.component.Button;
	import com.dedosmedia.component.Camera;
	import com.dedosmedia.component.CameraTexture;
	import com.dedosmedia.component.Gallery;
import com.dedosmedia.component.Scroller;
import com.dedosmedia.component.TextInput;
	import com.dedosmedia.component.Video;
	import com.dedosmedia.events.SoundEventType;
	import com.dedosmedia.model.AppModel;
	import com.dedosmedia.model.AssetsModel;

import feathers.events.MediaPlayerEventType;

import flash.filesystem.File;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.text.TextFormat;
	import flash.utils.getDefinitionByName;
	
	import feathers.controls.ButtonState;
	import feathers.controls.ImageLoader;
	import feathers.controls.Screen;
	import feathers.controls.TextInputState;
	import feathers.controls.text.TextFieldTextEditor;
	import feathers.controls.text.TextFieldTextRenderer;
	import feathers.core.FeathersControl;
	import feathers.core.ITextEditor;
	import feathers.core.ITextRenderer;
	import feathers.data.ListCollection;
	import feathers.events.FeathersEventType;
	import feathers.events.MediaPlayerEventType;
	import feathers.layout.HorizontalAlign;
	import feathers.skins.FunctionStyleProvider;
	import feathers.skins.ImageSkin;
	
	import org.robotlegs.starling.mvcs.Mediator;
	
	import starling.core.Starling;
	import starling.display.DisplayObject;
	import starling.display.DisplayObjectContainer;
	import starling.display.Image;
	import starling.display.MovieClip;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.textures.Texture;
	import starling.utils.StringUtil;
	
	public class CustomMediator extends Mediator
	{
		[Inject]
		public var assetsModel:AssetsModel;
		
		[Inject]
		public var appModel:AppModel;
		
		private var _screen:Screen;
		
		private var layout:Object;				// JSON con el layout de la pantalla
		
		private var _textures:Vector.<Texture> = new Vector.<Texture>();
		
		
		private var tempObj:Object;
		
		
		public function CustomMediator()
		{
			super();
		
			// se necesitan llamar estas CLases para q queden reconocidas por getDefinitionByName. Ya q la clase no se usa en el proyecto y no se embebe
			MovieClip;
			TextInput;
			Sprite;					
			Camera;
			CameraTexture;
			com.dedosmedia.component.Button;
			com.dedosmedia.component.TextInput;
			Scroller;
		}
		
		public function get screen():Screen
		{
			return this._screen;
		}
		
		/*
		public function set screen(value:Screen):void
		{
			
			trace("SCRENN "+value,this.getViewComponent())
			this._screen = value;
		}
		*/
		
		
		// Si se pasa un valor por defecto, en caso de no tener valor setea el default y retorna null. Si no se pasa default, en caso de no tener valor retorna un String indicando el error.
		// Comop se valida si el tipo de dato es correcto? por eje. Un Object 
		private function setProperty(obj:Object, index:Array, def:* = null):String
		{
			var temp:Object = obj;
			var temp2:Object;
			var error:Boolean =false;
			for(var i:uint = 0, l:uint = index.length; i<l;i++)
			{
				error = temp.hasOwnProperty(index[i])? false: true;
				if(error)
				{
					if(i == l-1)
					{
						if(def != null)
						{
							temp[index[i]] = def;
							return null
						}
						else
						{
							return showHint(index,obj.name)
						}
					}
					else
					{
						//objeto intermedio no existe. crearlo
						temp[index[i]] = new Object();
						temp = temp[index[i]]
					}
				}
				else
				{
					temp2 = temp[index[i]];
					if(typeof(temp2) != "object")
					{
						// llegamos al valor final... porner el def si lo hay
						if((temp2 as String) == "")
						{
							if(def != null)
							{
								temp[index[i]] = def;
							}
						}	
					}
					temp = temp[index[i]];
				}
			}
			// ya tenía un valor definido, pero es vacio? de ser así es incorrecto
			if((temp as String) =="")
			{
				return showHint(index,obj.name)
			}	
			// valor correcto
			return null
		}
		
		// concatenate error message
		private function showHint(arr:Array, name):String
		{
			return StringUtil.format("ERROR: missing or empty '{0}' property on item name: '{1}' on '{2}' config file.",arr.join("."),name, screen.screenID)
		}
		
		// Push error message to stack is answer is != from null
		private function pushError(arr:Array, answer:String):void
		{
			answer != null?arr.push(answer):"";
		}
		
		protected function getProperty(obj:Object, index:Array):*{
			var temp:Object = obj;
			var error:Boolean =false;
			for(var i:uint = 0, l:uint = index.length; i<l;i++)
			{
				error = temp.hasOwnProperty(index[i])? false: true;
				if(error)
				{
					return null;
				}
				else
				{
					temp = temp[index[i]];
				}
			}
			return temp
		}
		
		// Verifica el JSON para ver que tenga todas las propiedades o retorna true como error
		protected function checkProperty(obj:Object):Boolean
		{
			var errorStack:Array = new Array();
			var answer:String
			
			/*
			 * 
				REQUERIDO POR TODOS
				type 
				name 
				matrix.a .b .c .d .tx .ty
				w   -> No es necesario cuando el botón tiene una textura como imagen, ya que el ancho lo da la textura
				h   -> No es necesario cuando el botón tiene una textura como imagen
			*/
	
			
			if(!obj.hasOwnProperty("name"))
			{
				errorStack.push( StringUtil.format("ERROR: missing 'name' property on some item on '{0}' config file.",screen.screenID));
			}
			else if(!obj.hasOwnProperty("type"))
			{
				errorStack.push( StringUtil.format("ERROR: missing TYPE property on item name: '{0}' on '{1}' config file.",obj.name, screen.screenID));
			}
			
			// REQUERIDOS
			answer = setProperty(obj,["matrix","a"]);
			pushError(errorStack,answer);
			answer = setProperty(obj,["matrix","b"]);
			pushError(errorStack,answer);
			answer = setProperty(obj,["matrix","c"]);
			pushError(errorStack,answer);
			answer = setProperty(obj,["matrix","d"]);
			pushError(errorStack,answer);
			answer = setProperty(obj,["matrix","tx"]);
			pushError(errorStack,answer);
			answer = setProperty(obj,["matrix","ty"]);
			pushError(errorStack,answer);
		
			// defaults general
			setProperty(obj,["visible"],true); 
			setProperty(obj,["alpha"],1);
			setProperty(obj,["enabled"],true);
			setProperty(obj,["touchable"],false);
			
			
			// Dependiendo del type puede requerir unos cmapos obligatorios que en otro no lo sean... tenemos q definir eso
			switch(obj.type)
			{
				case "Button":
					//defaults
						setProperty(obj,["sound"],"");
				case "Image":
						// defaults
						setProperty(obj,["textures","default"],"");
						setProperty(obj,["textures","down"],""); 
						setProperty(obj,["textures","disabled"],""); 
						
						
						// requeridos
						var temp_texture:String = getProperty(obj,["textures","default"]);
						switch(getTextureType(temp_texture))
						{
							case "empty":
							case "color":
								// requiere w y h solo si es empty o color
								answer = setProperty(obj,["w"]);
								pushError(errorStack,answer);
								answer = setProperty(obj,["h"]);
								pushError(errorStack,answer);
								break;
							case "image":
								break;
						}
					break;
				case "Scroller":
					// defaults
					setProperty(obj,["textures","default"],"");
					setProperty(obj,["textures","down"],""); 
					setProperty(obj,["textures","disabled"],""); 
					
					
					// requeridos
					var temp_texture:String = getProperty(obj,["textures","default"]);
					switch(getTextureType(temp_texture))
					{
						case "empty":
						case "color":
							// requiere w y h solo si es empty o color
							answer = setProperty(obj,["w"]);
							pushError(errorStack,answer);
							answer = setProperty(obj,["h"]);
							pushError(errorStack,answer);
							break;
						case "image":
							break;
					}
					break;
				case "Gallery":
					// defaults
					setProperty(obj,["textures","default"],"");
					setProperty(obj,["textures","down"],""); 
					setProperty(obj,["textures","disabled"],""); 
					
					
					// requeridos
					var temp_texture:String = getProperty(obj,["textures","default"]);
					switch(getTextureType(temp_texture))
					{
						case "empty":
						case "color":
							// requiere w y h solo si es empty o color
							answer = setProperty(obj,["w"]);
							pushError(errorStack,answer);
							answer = setProperty(obj,["h"]);
							pushError(errorStack,answer);
							break;
						case "image":
							break;
					}
					break;
				case "Camera":
				case "CameraTexture":
					// defaults
					setProperty(obj,["textures","default"],"0xFF0000");
					setProperty(obj,["mode"],"noBorder");
					setProperty(obj,["frame"],[])
					// requeridos
					var temp_texture:String = getProperty(obj,["textures","default"]);
					
					switch(getTextureType(temp_texture))
					{
						case "empty":
						case "color":
							// requiere w y h solo si es empty o color
							answer = setProperty(obj,["w"]);
							pushError(errorStack,answer);
							answer = setProperty(obj,["h"]);
							pushError(errorStack,answer);
							break;
						case "image":
							break;
					}
					
					answer = setProperty(obj,["stream"]);
					pushError(errorStack,answer);
					
					answer = setProperty(obj,["viewport"]);
					pushError(errorStack,answer);
					
					break;
				case "Sprite":
						//defaults
						setProperty(obj,["item"],[]);
					break;
				case "Video":
					//defaults
					setProperty(obj,["autoPlay"],true);
					setProperty(obj,["stream"],"");
					
					break;
				case "TextInput":
					// defaults
					setProperty(obj,["textures","default"],"");
					setProperty(obj,["textures","focused"],""); 
					setProperty(obj,["textures","disabled"],""); 
					setProperty(obj,["textures","error"],"");
					setProperty(obj,["scale9grid"],[0,0,0,0]);
					setProperty(obj,["padding"],[0,0,0,0]);
					setProperty(obj,["embedFonts"],false); 
					setProperty(obj,["fontColor","default"],"0xFF0000");
					setProperty(obj,["fontColor","prompt"],"");
					setProperty(obj,["fontColor","error"],"");
					setProperty(obj,["fontColor","focused"],"");
					setProperty(obj,["fontColor","error"],"");
					setProperty(obj,["fontSize"],20); 
					setProperty(obj,["fontName"],"Arial"); 
					setProperty(obj,["textAlign"],"center"); 
					setProperty(obj,["restrict"],null); 
					setProperty(obj,["prompt"],""); 
					setProperty(obj,["error"],"");
					setProperty(obj,["required"],false);
					setProperty(obj,["validation"],"");
					setProperty(obj,["formatString"],"");
					 
					break;
				case "MovieClip":
						//defaults
						setProperty(obj,["sound"],[]);
						setProperty(obj,["loop"],true);
						
						// requeridos
						answer = setProperty(obj,["fps"]);
						pushError(errorStack,answer);
						
					break;
					
			}
			
			
		
			if(errorStack.length>0)
			{
				trace("ERROR:  HAY CAMPOS SIN VALOR... ",errorStack)
				return true;
			}
			
			return false;
		}
		
		
		private function getTextureType(_textureName:String):String
		{
			if(_textureName=="")
			{
				var _type:String = "empty";
			}
			else if(_textureName.indexOf("0x")!=-1)
			{
				_type = "color";
			}
			else
			{
				_type = "image";
			}
			
			return _type;
		}
		
		override public function onRegister():void
		{
			trace("*** ONREGISTER",screen)
			this._screen = this.getViewComponent() as Screen;
			layout = assetsModel.assets.getObject(screen.screenID);

            var v:Vector.<String> = assetsModel.assets.getXmlNames("s");
			trace("assetsModel.... "+v.toString())

			if(layout)
			{
				createItemFromJSON(layout,screen)
				
			}
			else
			{
				trace("ERROR: no se encuentra ",screen.screenID,".json, por favor revisar el nombre.")
			}
			

		}
		

		private function createItemFromJSON(layout:Object, _root:DisplayObjectContainer):void
		{
			var displayItem:DisplayObject;
			for(var i:uint = 0, l:uint = layout.item.length; i < l; i++)
			{
				var obj:Object 	= layout.item[i];
				
				if(checkProperty(obj) == true)
				{
					trace("ERROR DE CONFIGURACION.... SALIR")
					return;
				}
				
				// no poner elementos deshabilitados
				if(!obj.enabled) continue;
					
				switch(obj.type)
				{
					
					case "Button":
						var classType:Class = getDefinitionByName("com.dedosmedia.component.Button") as Class;
						displayItem  = new classType();
						
						Button(displayItem).sound = obj.sound;
						displayItem.addEventListener(Event.TRIGGERED, button_triggeredHandler);
						Button(displayItem).styleProvider =  new FunctionStyleProvider(function(_elem:Button):void{
							var skin:ImageSkin = new ImageSkin(getTexture(obj.textures["default"], obj.w, obj.h).texture);
							obj.textures["down"]!=""?skin.setTextureForState(ButtonState.DOWN,getTexture(obj.textures["down"], obj.w,obj.h).texture):null;
							obj.textures["disabled"]!=""?skin.setTextureForState(ButtonState.DISABLED,getTexture(obj.textures["disabled"], obj.w,obj.h).texture):null;
							_elem.defaultSkin = skin;
						});
						
						// por defecto
						obj.touchable = true;
						
						
						break;
					case "Camera":
						classType = getDefinitionByName("com.dedosmedia.component.Camera") as Class;
						displayItem  = new classType(Starling.current.nativeStage);
						
						
						tempObj = getTexture(obj.textures["default"], obj.w, obj.h)
						ImageLoader(displayItem).source = tempObj.texture;
						Camera(displayItem).streamRect = new Rectangle(0,0,obj.stream["width"], obj.stream["height"])
						Camera(displayItem).viewportRect = new Rectangle(0,0,obj.viewport["width"],obj.viewport["height"]);
						Camera(displayItem).viewportPosition = new Point(obj.viewport["x"],obj.viewport["y"]);
						Camera(displayItem).mode = obj.mode;
						//El index de la ´camra se debe leere de algun fichero de configuracion, que lo setee el usuario reciend arranque, si no está seteado mostrar opciones para setearlo
						Camera(displayItem).start("0", assetsModel.assets.getBitmapData(obj.textures["default"]));
						break;
					case "CameraTexture":
						classType = getDefinitionByName("com.dedosmedia.component.CameraTexture") as Class;
						displayItem  = new classType() 
						
						
						CameraTexture(displayItem).streamRect = new Rectangle(0,0,obj.stream["width"], obj.stream["height"])
						CameraTexture(displayItem).viewportRect = new Rectangle(0,0,obj.viewport["width"],obj.viewport["height"]);
						CameraTexture(displayItem).viewportPosition = new Point(obj.viewport["x"],obj.viewport["y"]);
						// CameraTexture(displayItem).mode = obj.mode;  //no necesario, solo se permite noBorder
						//El index de la ´camra se debe leere de algun fichero de configuracion, que lo setee el usuario reciend arranque, si no está seteado mostrar opciones para setearlo
						
						if(obj.frame)
						{
							
							var frame:Vector.<Texture> = new Vector.<Texture>();
							var texture:Texture;
							for(var j:uint = 0, k:uint = obj.frame.length; j < k; j++)
							{
								texture = getTexture(obj.frame[j].texture).texture;
								frame.push(texture);
							}
						}
						
						CameraTexture(displayItem).start("0", frame);
						
						break;
					case "Scroller":
						classType = getDefinitionByName("com.dedosmedia.component.Scroller") as Class;
						displayItem  = new classType();
						
						tempObj = getTexture(obj.textures["default"], obj.w, obj.h)
						Scroller(displayItem).backgroundSkin = new Image(tempObj.texture);
						Scroller(displayItem).gap = obj.gap			
						
						var _image:Array = new Array();
						for(var y:int = 0, y_max:int = obj.item.length; y < y_max; y++)
						{
							var temp:ImageLoader = new ImageLoader();
							temp.source = getTexture(obj.item[y].texture).texture
							_image.push(temp);
						}

						Scroller(displayItem).item = _image;
						
						displayItem.width = Texture(tempObj.texture).nativeWidth;
						displayItem.height = Texture(tempObj.texture).nativeHeight;

						break;
					case "Gallery":
						classType = getDefinitionByName("com.dedosmedia.component.Gallery") as Class;
						displayItem  = new classType();
						
						tempObj = getTexture(obj.textures["default"], obj.w, obj.h)
						Gallery(displayItem).backgroundSkin = new Image(tempObj.texture);
						Gallery(displayItem).gap = obj.gap			
						
					
						var collection:ListCollection = new ListCollection();
						for(var y:int = 0, y_max:int = obj.item.length; y < y_max; y++)
						{
							//collection.push({ texture: getTexture(obj.item[y].texture).texture, bio:obj.item[y].bio})
							var il:ImageLoader = new ImageLoader();
							il.source = getTexture(obj.item[y].texture).texture;
							collection.push({ texture: il, bio:obj.item[y].bio})
								
								// AQUI PUEDE HABER MEMORIA Q DEBO BORRAR DE LAS TEXTURAS
						}
						Gallery(displayItem).dataProvider = collection;
						
						
						//displayItem.touchable = false;
						displayItem.addEventListener(Event.TRIGGERED, list_triggeredHandler);
						displayItem.width = Texture(tempObj.texture).nativeWidth;
						displayItem.height = Texture(tempObj.texture).nativeHeight;
						
						// por defecto
						obj.touchable = true;
						break;

					case "Image":
						classType = getDefinitionByName("feathers.controls.ImageLoader") as Class;
						displayItem  = new classType();
						ImageLoader(displayItem).source = getTexture(obj.textures["default"], obj.w, obj.h).texture;	
						break;
					case "Sprite":						
						classType = getDefinitionByName("starling.display.Sprite") as Class;
						displayItem  = new classType();
						createItemFromJSON(obj, DisplayObjectContainer(displayItem))
						break;

					case "Video":
						classType = getDefinitionByName("com.dedosmedia.component.Video") as Class;
						var _url:String = obj.stream;
						
						// Si no es web, debe ser un File
						if(obj.stream.indexOf("http://") == -1)
						{
							_url = appModel.projectDirectory.resolvePath(obj.stream).url
						}   
												
						displayItem  = new classType();
						
						Video(displayItem).styleProvider = null;
						Video(displayItem).autoPlay = obj.autoPlay;
						displayItem.addEventListener( Event.READY, videoPlayer_readyHandler );
						displayItem.addEventListener( feathers.events.FeathersEventType.CLEAR, videoPlayer_clearHandler );
						displayItem.addEventListener(Event.COMPLETE, videoPlayer_completeHandler);
						displayItem.addEventListener(MediaPlayerEventType.PLAYBACK_STATE_CHANGE, videoPlayer_playbackStateChangeHandler);
						
						if(_url != "")
						{
							Video(displayItem).start(_url);
						}
						
						
						break;
					
					case "MovieClip":
						classType = getDefinitionByName("starling.display.MovieClip") as Class;
						try
						{
							displayItem  = new classType( assetsModel.assets.getTextures(obj.textures["default"]), obj.fps);
						}
						catch(e:ArgumentError)
						{
							trace("XXXXXXXX ERROR: Textura para  el movieclip no existe")
							return;
						}
						if(obj.sound)
						{
							var sound_arr:Array = obj.sound;
							for(var j:uint = 0, k:uint = sound_arr.length; j < k; j++)
							{
								try{
									MovieClip(displayItem).setFrameSound(sound_arr[j].frame, assetsModel.assets.getSound(sound_arr[j].sound));
								}
								catch(e:ArgumentError)
								{
									trace("ERROR: Está intentando agregar un sonido a un frame inexistente "+e.message);
								}
							}
						}
						displayItem.addEventListener(Event.COMPLETE, movieClip_completeHandler);
						MovieClip(displayItem).loop = obj.loop;
						break;
					case "TextInput":  
						classType = getDefinitionByName("com.dedosmedia.component.TextInput") as Class;
						displayItem  = new classType();
						
						tempObj = getTexture(obj.textures["default"], obj.w,obj.h);
						TextInput(displayItem).styleProvider = null;
						
						TextInput(displayItem).styleProvider =  new FunctionStyleProvider(function(_elem:TextInput):void{
							
							trace("TEXTURE ",tempObj.texture, Texture(tempObj.texture).frameWidth,Texture(tempObj.texture).nativeWidth, Texture(tempObj.texture).width)
							
							//var skin:ImageSkin = new ImageSkin(tempObj.texture);
							var skin:ImageSkin = new ImageSkin();
							
							skin.scale9Grid = new Rectangle(obj.scale9grid[0],obj.scale9grid[1],obj.scale9grid[2],obj.scale9grid[3]);
							    
							
							var objTexture:Object = getTexture(obj.textures["default"], obj.w,obj.h);
							objTexture.textureType == "color"? skin.setColorForState(TextInputState.ENABLED, parseInt(obj.textures["default"],16)):null;
							objTexture.textureType == "image"? skin.setTextureForState(TextInputState.ENABLED,objTexture.texture):null;
							
							var objTexture:Object = getTexture(obj.textures["disabled"], obj.w,obj.h);
							objTexture.textureType == "color"? skin.setColorForState(TextInputState.DISABLED, parseInt(obj.textures["disabled"],16)):null;
							objTexture.textureType == "image"? skin.setTextureForState(TextInputState.DISABLED,objTexture.texture):null;
							
							objTexture = getTexture(obj.textures["focused"], obj.w,obj.h);
							objTexture.textureType == "color"? skin.setColorForState(TextInputState.FOCUSED, parseInt(obj.textures["focused"],16)):null;
							objTexture.textureType == "image"? skin.setTextureForState(TextInputState.FOCUSED,objTexture.texture):null;
							
							objTexture = getTexture(obj.textures["error"], obj.w,obj.h);
							trace("ERROR TEXTURE:",objTexture.textureType,obj.textures["error"])
							objTexture.textureType == "color"? skin.setColorForState(TextInputState.ERROR, parseInt(obj.textures["error"],16)):null;
							objTexture.textureType == "image"? skin.setTextureForState(TextInputState.ERROR,objTexture.texture):null;
							
							//skin.setColorForState(TextInputState.ERROR, 0x666666);
							
							_elem.backgroundSkin = skin;
						});   
						
						TextInput(displayItem).textEditorFactory = function():ITextEditor
						{
							var textEditor:TextFieldTextEditor = new TextFieldTextEditor();
							textEditor.styleProvider = null;
							
							var textEnabled:TextFormat = new TextFormat(obj.fontName, obj.fontSize, obj.fontColor["default"] );
							textEnabled.align = obj.textAlign;
							
							var textDisabled:TextFormat = new TextFormat(obj.fontName, obj.fontSize, obj.fontColor["disabled"]  );
							textDisabled.align = obj.textAlign;
							
							var textError:TextFormat = new TextFormat(obj.fontName, obj.fontSize, obj.fontColor["error"]  );
							textError.align = obj.textAlign;
							
							var textFocused:TextFormat = new TextFormat(obj.fontName, obj.fontSize, obj.fontColor["focused"]  );
							textFocused.align = obj.textAlign;							
							
							textEditor.setTextFormatForState( TextInputState.ENABLED, textEnabled);
							textEditor.setTextFormatForState( TextInputState.DISABLED,textDisabled);
							textEditor.setTextFormatForState( TextInputState.ERROR, 	textError);
							textEditor.setTextFormatForState( TextInputState.FOCUSED,	textFocused);
							textEditor.embedFonts = obj.embedFonts;
							return textEditor;
						}
						
						TextInput(displayItem).promptFactory = function():ITextRenderer
						{
							var textRenderer:TextFieldTextRenderer = new TextFieldTextRenderer();
							
							textRenderer.styleProvider = null;
							var format:TextFormat = new TextFormat(obj.fontName, obj.fontSize, obj.fontColor["prompt"] );
							format.align = obj.textAlign;				
							textRenderer.textFormat = format;
							textRenderer.embedFonts = obj.embedFonts;
							return textRenderer;
						};
						
						displayItem.addEventListener(FeathersEventType.FOCUS_OUT, textInput_focusOutHandler )
						displayItem.addEventListener(Event.CHANGE, textInput_changeHandler )
						
						TextInput(displayItem).paddingLeft = 	obj.padding[0];
						TextInput(displayItem).paddingTop = 	obj.padding[1];
						TextInput(displayItem).paddingRight = 	obj.padding[2];
						TextInput(displayItem).paddingBottom =	obj.padding[3];
						
						TextInput(displayItem).validation =	obj.validation;
						TextInput(displayItem).required =	obj.required;
						
						
						TextInput(displayItem).prompt = obj.prompt;
						TextInput(displayItem).error = obj.error;
						TextInput(displayItem).formatString = obj.formatString;
						TextInput(displayItem).restrict = obj.restrict;
						
						//displayItem.width = Texture(tempObj.texture).nativeWidth;  // el tamaño real de la textura? y si es escaaldo con scel9grid
						//displayItem.height = Texture(tempObj.texture).nativeHeight;
						
						displayItem.width = obj.w;  // el tamaño real de la textura? y si es escaaldo con scel9grid
						displayItem.height = obj.h;
						
						//touchable
						obj.touchable = true;
						break;
				}
				
				displayItem.touchable = obj.touchable;
				displayItem.name = obj.name;
				displayItem.alpha = obj.alpha;
				displayItem.visible = obj.visible
				_root.addChild(displayItem);
				
				
				trace("   [Component name] ",obj.name," type:",displayItem)
				
				
				try
				{
					
					FeathersControl(displayItem).validate();
				}
				catch(e:Error)
				{
					trace(" NO EXISTE VALIDATE")
				}
				
				var matrix:Matrix = new Matrix(obj.matrix.a,obj.matrix.b,obj.matrix.c,obj.matrix.d,obj.matrix.tx,obj.matrix.ty);
				
				switch(obj.type)
				{
					case "Video":
							matrix.a = matrix.a/assetsModel.assets.scaleFactor;
							matrix.d = matrix.d/assetsModel.assets.scaleFactor;
		
						break;
				}
						
				matrix.tx = matrix.tx/assetsModel.assets.scaleFactor;
				matrix.ty = matrix.ty/assetsModel.assets.scaleFactor;
				
				
				DisplayObject(displayItem).transformationMatrix = matrix;

				
				//mc = DisplayObject(displayItem);
				

				
				
			}
		}
		

		// pasado un string devuelve una textura y su tipo. Imagen, color
		private function getTexture(_textureName:String, _w:Number = 0, _h:Number = 0):Object{
			var scale:Number = assetsModel.assets.scaleFactor;
			var __texture:Texture
			
			if(_textureName=="")
			{
				var _type:String = "empty";
			}
			else if(_textureName.indexOf("0x")!=-1)
			{
				_type = "color";
			}
			else
			{
				_type = "image";
			}
			
			switch(_type) 
			{
				
				case "empty":
					__texture = Texture.empty(_w/scale,_h/scale);
					_textures.push(__texture);
					break;
				case "color":
					__texture = Texture.fromColor(_w/scale,_h/scale,parseInt(_textureName,16),1);
					_textures.push(__texture);
					break;
				case "image":
					__texture = assetsModel.assets.getTexture(_textureName)
					break;
			}
			return {texture:__texture, textureType:_type};
		}
	
		
		//sobreescribir
		protected function button_triggeredHandler(e:Event):void
		{
			if(Button(e.currentTarget).sound != "")
			{
				this.dispatchWith(SoundEventType.PLAY_SOUND,true,Button(e.currentTarget).sound);
			}
			
			
		}
		
		protected function list_triggeredHandler(e:Event):void
		{
		
		}
		
		//sobreescribir
		protected function videoPlayer_playbackStateChangeHandler(e:Event):void
		{
			
		}
		
		protected function movieClip_completeHandler(e:Event):void
		{
			
		}
		
		protected function textInput_focusOutHandler(e:Event):void
		{
			
		}
		
		protected function textInput_changeHandler(e:Event):void
		{
			
		}
		
		protected function videoPlayer_clearHandler(e:Event):void
		{
			
		};
		
		protected function videoPlayer_readyHandler(e:Event):void
		{
			trace("ready")
		};
		
		protected function videoPlayer_completeHandler(e:Event):void
		{
			trace("completed")
		};
		

		
		
		override public function onRemove():void
		{
			trace("onRemove::CustomMediator ",screen)
			for(var i:uint = 0, l:uint = _textures.length; i < l ; i++)
			{
				_textures[i].dispose();
				_textures[i] = null;
			}
			_textures = null;
			
			
			while(screen.numChildren>0)
			{
				trace("remove "+screen.getChildAt(0))
				screen.removeChild(screen.getChildAt(0),true);
			}
			
			
			trace("onRemove::Mediator")
			trace("HAY EU REMOVER LOS LISTENER DE CADA OBJETO???? EN CUSTOM MEDIATOR")
			super.onRemove();
			
		
		}
		
		 
	}
}