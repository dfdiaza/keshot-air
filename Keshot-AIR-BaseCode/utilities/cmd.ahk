try{
	Process,  exist, AfterFX.exe
	
	pid = %ErrorLevel%
	if %pid%
	{
		WinHide, ahk_pid %pid%
		
	}
	else
	{
		Run, "shortcut.lnk"
		Process,  exist, AfterFX.exe
		pid = %ErrorLevel%	
		WinWaitActive, ahk_pid %pid%
		WinShow, ahk_pid %pid%
		WinMinimize, ahk_pid %pid%
		WinHide, ahk_pid %pid%
		Sleep 1000
		WinHide, ahk_pid %pid%
	}

	Exit 0
}
catch e {
	; No existe el Shortcut
	MsgBox "ERROR"
	MsgBox %e%
	Exit 1
}